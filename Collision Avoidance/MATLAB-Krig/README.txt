*************************************************************************************
*   eTSSO: 
*    Two Stage Algorithm for Stochastic Global Optimization Under Finite Budget
************************************************************************************* 

REFERENCE: Liu, C., Pedrielli, G. and Ng, S.H. (2014)
----------

=======================================================================================
The one-dimensional and multi-dimensional simulation optimization code is written in Matlab.

This code provides an extended sequential two-stage algorithm for the optimization of 
a stochastic computer model.  
It provides some function example (f.m, f2d, f3d, f6d files) to illustrate the execution. 
As it is a sequential design and run approach, after the initial designed experiment, 
further observation data are generated from the test function according to the sequential
search and allocation approach.


The code relies upon the MNEK model (Yin et al. 2011) to fit the response surface.

=======================================================================================

********************
LIST OF MATLAB FILES
********************

File name: eTSSO_1d.m, eTSSO_kd.m

Description:
Runs the two stage algorithm for  minimization problem for the 1-dimensional case and multiple dimensions case, respectively. 

File name: MNEK_model.m, MNEK_corr.m, MNEK_regr.m, MNEK_lh.m, MNEKmse_1d.m (MNEK_model_kd.m, MNEK_lh_kd.m,MNEKmse_kd.m for the general k-dimensional case, with k>1)
Description:
Files used for fitting a Modified Nugget Effect Kriging model to model the output.

File name: cv_1d.m (cv_kd.m)
Description:
Implements the cross-validation algorithm for the 1-dimenensional and k-dimensional problem, respectivley.
Note: Cross-validation results appear in the vector "cross_val". A vector entry of "1" indicates a failed cross-validation test for the point with the same index in the input vector. 

File name: MNEK_predict.m
Description: used for the response prediction

File name: EIcalc_MNEK_kd.m
Description:
Calculates the modified Expected Improvement value(s) for a vector of location(s).

File name: OCBA.m
Description:
Carries out OCBA for given input vectors of sample means, sample variances and available budget.   

File name: f.m, fkd.m
Description:
test functions used for illustration purposes having 1 (Quan et al. 2013), 2 (tetra-modal) dimensions, respectively.

Function calls to these can be replaced with calls to execute the computer code at the specified design points. 




*******************************
USAGE OF CODE FOR TEST FUNCTION
*******************************

1. Enter the required test function in the f.m file in case you are using eTSSO_1d, if using eTSSO_kd enter the function in "fkd.m" file.  Alternatively, a call to the computer model is required;
 
2. In the eTSSO_1d.m (eTSSO_kd.m) file, input the user defined parameters.  Then run the code. NO parameters need to be provided as input to the remaining files;

3. x_axis_2d.dat provides the grid for running eTSSO with the 2d sample function. The grid is provided only for the sake of keeping the same experimental conditions;

4. "x_0_2d.dat" is the initial design for the provided 2-d test function. Also in this case, the design is given for the sake of the experimental conditions;

5. The eTSSO_kd.m is currently set for a 2d case (Tetra-Modal function). Both "x_axis_2d.dat" as well as "x_0_2d.dat" are loaded by the program and the minimum of the tetramodalfunction is computed;

6. results are stored in the "y_res" matrix which has the following columns: 
i - T - x(k)-x(k)* - y-y* - |x-x*| - K* - B* - SC
where
i : macro-replication index;
T - total budget allocated to the eTSSO
x(k) - x(k)* : difference between the best solution found by TSSO in the k-th dimension and the true best value in the k-th dimension this difference can be positive or negative and a number of colums is defined equal to the dimension of the input;
y-y* : difference between the final best function value identified by eTSSO and the true minimum. This value can be either positive or negative;
|x-x*| : euclidean distance between the estimated and the true optimum location (only positive);
K* : iteration at which the best estimated location is identified;
B* : budget used up to iteration K* (included)
SC : in case you are using the stopping criterion, this flag will be set to 1 if the eTSSO stops because the criterion is met, it is 0 in case either the criterion is not used, or it is used but not met.

********************
REFERENCES
********************

[1] Ning Quan, Jun Yin, Szu Hui Ng, and Loo Hay Lee. 2013. Simulation optimization via kriging: a sequential search using expected improvement with computing budget constraints. Iie Transactions 45, 7 (2013),763�780.
[2] Jun Yin, Szu Hui Ng, and Kien Ming Ng. 2011. Kriging metamodel with modified nugget-effect: The heteroscedastic variance case. Computers & Industrial Engineering 61, 3 (2011), 760�777.