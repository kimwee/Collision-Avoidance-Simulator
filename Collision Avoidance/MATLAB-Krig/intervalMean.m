function c_mean = intervalMean(x, interval)
    x = sortrows(x, 1);
    fmin = min(x(:,1));
    fmax = max(x(:,1));
    c_mean = 0;
    %own_idx = 1;
    orig_idx = 1;
    for i = fmin+interval:interval:fmax
       temp = 0;
       while x(orig_idx,1) <  i && orig_idx < length(x)
           temp(length(temp)+1) = x(orig_idx, 2);
           orig_idx = orig_idx + 1;
       end
      
       if length(temp) > 1
           own_idx = length(c_mean)+1;
           c_mean(own_idx,1) = i;
           c_mean(own_idx,2) = mean(temp(1:end));
           %own_idx = own_idx+1;
       end
    end
    c_mean(1,:) = [];
end

    