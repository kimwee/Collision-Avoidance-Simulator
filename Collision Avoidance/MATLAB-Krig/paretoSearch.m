function [yy1, yy2, xx] = paretoSearch(y1, y2, x, counter)
    yy1 = 0;
    yy2 = 0;
    xx = zeros(1, 4);
    status = zeros(counter, 1);%0 for non-dominated, 1 for dominated
    for i = 1:counter
        for j = 1:counter
            if (y1(i) > y1(j) && y2(i) > y2(j))
                status(i) = 1;
                break;
            end
        end
        if status(i) == 0
            yy1 = [yy1; y1(i)];
            yy2 = [yy2; y2(i)];
            xx = [xx; x(i,:)];
        end
    end
    yy1(1,:) = [];
    yy2(1,:) = [];
    xx(1,:) = [];
end
    