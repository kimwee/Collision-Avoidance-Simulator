function krig = krig(bTime)
    %clear all; close all; clc
    warning('off', 'all');
    T_setting=750;
    r_min_setting=25;
    tic;
    u = 5;
    safety = 0.04;
    for gun = 1:1
    eTSSO_kdn;
    clearvars -except bTime minProbn zargn gun minDistn T_setting r_min_setting u safety;
    eTSSO_kdp;
    clearvars -except bTime minProbn zargn minProbp zargp gun minDistp minDistn T_setting r_min_setting u safety;
    if max(minProbp, minProbn) < safety
        if minDistp < minDistn
            krig = zargp;
        else
            krig = zargn;
        end
    else %one or both more than threshold 
        if minProbp < minProbn
            krig = zargp;
        else
            krig = zargn;
        end
    end
    clearvars -except bTime minProbn zargn minProbp zargp gun krig T_setting r_min_setting u safety;
    %gun
    end
    t= toc;
    load('time.mat');
    k = size(tim,1);
    tim(k+1, 1) = t;
    save('time.mat', 'tim');
end