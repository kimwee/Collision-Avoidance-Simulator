function krig = krigd(bTime, rep, testcase)
    %clear all; close all; clc
    warning('off', 'all');
    T_setting=750;
    r_min_setting=25;
    tic;
    u = 5;
    safety = 0.04;
    for gun = 1:1
    eTSSO_kdn;
    clearvars -except bTime minProbn zargn gun minDistn T_setting r_min_setting u safety rep testcase;
    eTSSO_kdp;
    clearvars -except bTime minProbn zargn minProbp zargp gun minDistp minDistn T_setting r_min_setting u safety rep testcase;
    if max(minProbp, minProbn) < safety
        if minDistp < minDistn
            krig = zargp;
            dist = minDistp;
        else
            krig = zargn;
            dist = minDistn;
        end
    else %one or both more than threshold 
        if minProbp < minProbn
            krig = zargp;
            dist = minDistp;
        else
            krig = zargn;
            dist = minDistn;
        end
    end
    clearvars -except bTime minProbn zargn minProbp zargp gun krig T_setting r_min_setting u safety dist rep testcase;
    %gun
    end
    t= toc;
    load('time.mat');
    k = size(tim,1);
    tim(k+1, 1) = t;
    save('time.mat', 'tim');
    filename = 'Data\\distance';
    varname = 'dista';
    load([filename num2str(testcase) 'R' num2str(rep) '.mat']);
    k = size(dista, 1);
    dista(k+1, 1) = dist;
    save([filename num2str(testcase) 'R' num2str(rep) '.mat'], varname);
end