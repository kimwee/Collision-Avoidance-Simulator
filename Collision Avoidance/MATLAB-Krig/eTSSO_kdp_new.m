%function zarg = eTSSO_kdp(~)
% runs the two stage algorithm for a one-dimensional minimization example;
%clear all; close all; clc
%%Initialization
epsi = 0.000000;
%bTime = 8;
state = 12220;
randn('state',state);
ymean = 0;
yvar = 0;
% function under analysis
fhandle = @fkdp; % test function handle;
%function is defined in the file f.m;
%calls to this function can be replaced with calls to the simulation model
%Generating the true function for comparison purposes

%dimension of the function
d=4;%SET DIMENSION

for i=1:d
    x_min(i,1)=0;
    x_max(i,1)=1;
end

maxDIST=max(sqrt(sum((x_max-x_min).^2)));

MAXIMUM=2500;

%generate initial input grid
x_axis = lhsdesign(MAXIMUM,d);
tempDOEgrid=zeros(MAXIMUM,d);
for i=1:MAXIMUM
    tempDOEgrid(i,:)=(x_min+x_axis(i,:)'.*(x_max-x_min))';
end
x_axis=tempDOEgrid;

%if you want to set the input grid common to all experiments, then set
%initial_grid = 1.
initial_grid = 0;

if initial_grid == 1
    load('x_axis_2d.mat', 'x_axis');
end

% y_true=zeros(size(x_axis,1),1);
%
% for j = 1:size(x_axis(:,1))
%     y_true(j,1)=fhandle(x_axis(j,:));
% end
%
% [yStar xStarInd] = min(y_true);
% %manually input the xStar coordinates
% xStar(1,:) = [0.85 0.5 ];%0.85 0.5];
%
% yStar(1,1)=fhandle(xStar(1,:));
%
% %change the noise level q1 if necessary
% q1=1.0;
% deltaNoise=1.0;%use this to linearly increase the noise level
%
% %Noise function ATT'N REPLACE IT WITH THE DESIRED NOISE FUNCTION
% noise_f = @(x) deltaNoise*q1*(abs(x(1))+abs(x(2))+0.0000001);
% stopCritMET=0;%control on the stopping criterion

% user defined parameters--------------------------------------------------
a=0.01;%tolerance lavel for the stopping criterion
quadratic=0; %set to 1 for a quadratic increase in the number of replications
stopCriterionINUSE=0;%put 0 if you want to bring the optimization without stopping criterion

%%single observation
MAXITERCROSS=1; %maximum number of cross validation iteration you want to perform
alpha = 1.25; % single sided sigma limit for cross validation


%%-------------------------------------------------------------------------
%%--------------------------------ETSSO------------------------------------
%%-------------------------------------------------------------------------
macroReplications=1; %number of macro-replicationS
%initialize parameters here
%r_min_setting=20;
B_setting=r_min_setting; %B for the search procedure
%T_setting=600;
B_n0_setting=r_min_setting;
deltaB=0;
delta=0;%Used to increase the sample path size at each iteration

%for TSSO, budgetIncrease = 0; for eTSSO, budgetIncrease = 1
budgetIncrease=1;
%generate the initial design common to all macro-replications
%sample size of initial observations
n_0 = 20; % number of initial design points
x_0 = lhsdesign(n_0,d);
tempDOE=zeros(n_0,d);
for i=1:n_0
    tempDOE(i,:)=(x_min+x_0(i,:)'.*(x_max-x_min))';
end
x_0=tempDOE;

%if you want to set the initial designs common to all experiments, then set
%initial_design = 1.
initial_design = 0;
if initial_design == 1
    load('x_0_2d.mat', 'x_0');
end

%u = 4;%denotes number of points sampled per iteration
i_max = floor((T_setting-B_n0_setting*n_0)/(B_setting*u));
%variables used to collect statistics
y_res=zeros(macroReplications,7+d);
ystarDiff=zeros(macroReplications,1);
xstarDiff=zeros(macroReplications,d);
xstarDiffTOT=zeros(macroReplications,1);
stopCritMETvec=zeros(macroReplications,1);
solutionperRep=zeros(macroReplications,1);
usedBudget=zeros(macroReplications,1);
bestX=zeros(macroReplications,d);
Kstar=zeros(macroReplications,1);
BestBudget=zeros(macroReplications,1);
%%Initialize vectors
xStarE_i=zeros(i_max,d,macroReplications);
yStarE_i=zeros(i_max,macroReplications);

Bit=zeros(macroReplications,i_max);

for kk=1:macroReplications
    T = T_setting; % total number of replications
    B_n0=B_n0_setting; % number of replication available for the initial fit;
    B=B_setting;
    observations=zeros(2500,T,2);
    %sample size of initial observations
    r_min = r_min_setting; % minimum number of replications needed for one
    counter = n_0;
    partitions = size(x_axis,1);
    b = ones(counter,1);
    b_p = ones(partitions,1);
    
    model1 = MNEK_model_kd(x_cand(1:counter,:),y1(1:counter),v1(1:counter),0,2);
    model2 = MNEK_model_kd(x_cand(1:counter,:),y2(1:counter),v2(1:counter),0,2);
    [bestY bestXInd] = min(y1(1:counter));
    bestXtemp = x_cand(bestXInd,:);
    [yy1, yy2, xx] = paretoSearch(y1, y2, x_cand, counter);
    %     for InddimMin=1:size(xStar,1)
    %         %%compute distance from the optimum
    %         myDistance=bestXtemp-xStar(InddimMin,:);
    %         mySqDist=myDistance.^2;
    %         mySqrtSqDist=sqrt(sum(mySqDist));
    %         distanceTemp(InddimMin)=mySqrtSqDist;
    %     end
    %     distanceStar=min(distanceTemp);
    %     Kstartemp=n_0;
    
    %%% Performing the sequential two-stage algorithm
    i=1;
    %%set the  initial B
    if kk > 1 && deltaB>0
        B=B+deltaB;
    elseif budgetIncrease==1
        %%perform the initialization for the case of budget increase
        B=r_min;
    end
    i_max = floor((T-B_n0*n_0)/(u*B));
    i_max_init = i_max;
    
    SqrxdiffStarIteration=zeros(i_max,d);
    
    BestBudgetTemp=T-B_n0*n_0;
    r_A = zeros(i_max,1); % allocation stage budget
    r_S = zeros(i_max,1); % sampling stage budget
    if (i_max+n_0) > MAXIMUM
        'warning: total design points exceeds maximum'
        exit
    end
    T=T-n_0*B_n0;
    stopCritMET=0;
    
    [yy1, yy2, xx] = paretoSearch(y1, y2, x_cand, counter);
    curr_bestn = inf;
    for j = 1:counter
        if(y1(j) <  curr_bestn)
            curr_bestn = y1(j);
        end
    end
    
    %[EI_values Varextrinsic] = EIcalc_MNEK_kd(x_axis,x(1:counter,:),model1, curr_bestn);
    %[EI_values Varextrinsic] = MOEIcalc_MNEK_kd(x_axis,x(1:counter,:),model1, model2, [yy1 yy2]);
    [index Varextrinsic] = MOEI_MNEK_kd(x_axis,x_cand(1:counter,:),model1, model2, [yy1 yy2], [y1(1:counter,:) y2(1:counter,:)], 1);
    zargp = x_axis(index,:);
    %[maxEIk index] = max(EI_values);
    
    %%compute the terms you need for the new budget allocation rule
    
%     [BESTY index1] = min(y1(1:counter));
%     [WORSTY index2] = max(y1(1:counter));
%     v_BARint = v1(index1);
%     maxV_barInt=max(v1(1:counter));
%     v_BARext = Varextrinsic(index(1,1)).^2;
%     
%     %compute the r_A(i)
%     if i>1 && delta==0 && budgetIncrease==0
%         r_A(i) = r_A(i-1) + min(floor((B-r_min)/i_max),T-(i-1)*B);
%     elseif i>1 && (delta>0 || budgetIncrease==1)
%         r_A(i) = B-r_min;
%     end
%     
%     
%     
%     
%     
%     
%     if (B >= r_min && budgetIncrease==1) || (T-usedBudget(kk)-r_A(i) > 0 && budgetIncrease==0)
%         %verify the stopping criterion
%         [BESTY index1] = min(y1(1:counter));
%         [WORSTY index2] = max(y1(1:counter));
%         if delta==0 && budgetIncrease==0
%             
%             r_S(i) = B - r_A(i);
%         elseif (delta>0 || budgetIncrease==1)
%             r_S(i) = r_min;
%             
%             
%         end
%         for l = 1:size(index,1)%or u
%             if (stopCritMET*stopCriterionINUSE==0 && r_S(i)>0)
%                 x_new = x_axis(index(l,1),:);
%                 T_X = fhandle(x_new(1,:), r_S(i), bTime);%normrnd(fhandle(x_new(1,:)),noise_f(x_new(1,:)),r_S(i),1);
%                 y1_new = mean(T_X(:,1));
%                 y2_new = mean(T_X(:,2));
%                 v1_new =var(T_X(:,1))/r_S(i)+ epsi;
%                 v2_new =var(T_X(:,2))/r_S(i)+ epsi;
%                 counter = counter + 1;
%                 x(counter,:) = x_new;
%                 y1(counter) = y1_new;
%                 y2(counter) = y2_new;
%                 v1(counter) = v1_new;
%                 v2(counter) = v2_new;
%                 rep_cur(counter) = r_S(i);
%                 %ymean = [ymean; y_new]; yvar = [yvar;, v_new];
%                 %vertcat(ymean, y_new); vertcat(yvar, v_new);
%                 for k=1:r_S(i)
%                     observations(counter,k, 1)=T_X(k,1);
%                     observations(counter,k, 2)=T_X(k,2);
%                     
%                 end
%             end
%         end
%     end
%     
%     if(stopCritMET*stopCriterionINUSE==0)
%         %%compute the budget needed based on the indications from OCBA
%         OCBA_output_1 = OCBA(rep_cur(1:counter),y1(1:counter),v1(1:counter),r_min);
%         added_rep_OCBA = OCBA_output_1-rep_cur(1:counter,1);
%         [ocbaPoint indexOCBA]=max(added_rep_OCBA);
%         if i==1
%             remaining_budget=T-usedBudget(kk);
%         end
%         
%         if i>1 && budgetIncrease==1
%             var_ref=v1(indexOCBA);
%             nat_increase=floor(B + B*(var_ref*10000000/(v_BARext*10000000+var_ref*10000000)));
%             applied_increase=nat_increase;
%             
%             B = min(applied_increase,T-usedBudget(kk));
%             if(T-usedBudget(kk)-B<=B)
%                 B=T-usedBudget(kk);
%                 remaining_budget=0;
%             else
%                 remaining_budget=T-usedBudget(kk)-B;
%             end
%         end
%         Bit(kk,i)=B;
%         if remaining_budget>0 && budgetIncrease==1
%             i_max=floor((T-usedBudget(kk)-B)/(u*min(B,T-usedBudget(kk)-B)));
%         elseif remaining_budget==0 && budgetIncrease==1
%             i_max=1;
%         end
%         
%         %%Evaluation Procedure
%         if budgetIncrease==1
%             %                     size_r_A = size(r_A)
%             %                     size_r_S = size(r_S)
%             %                     i_max
%             %                     i
%             %                     r_A(i)
%             %                     B
%             %                     r_S(i)
%             r_A(i)=B*u-r_S(i);
%         end
%         
%         usedBudget(kk)=usedBudget(kk)+r_S(i)+r_A(i);
%         if T-usedBudget(kk)<B
%             r_A(i)=r_A(i)+T-usedBudget(kk);
%             usedBudget(kk)=T;
%         end
%         OCBA_output = OCBA(rep_cur(1:counter),y1(1:counter),v1(1:counter),r_A(i));
%         
%         for j = 1:counter
%             if (OCBA_output(j) > rep_cur(j))
%                 add_rep = OCBA_output(j) - rep_cur(j);
%                 T_D = fhandle(x(j,:), add_rep, bTime);%normrnd(fhandle(x(j,:)),noise_f(x(j,:)),add_rep,1);
%                 while(size(T_D, 1) ~= add_rep)
%                     T_D = fhandle(x(j,:), add_rep, bTime);
%                 end
%                 for k=rep_cur(j)+1:rep_cur(j)+add_rep
%                     
%                     observations(j,k,1 )=T_D(k-rep_cur(j),1);
%                     observations(j,k,2 )=T_D(k-rep_cur(j),2);
%                 end
%                 y1(j)=mean(observations(j,1:rep_cur(j)+add_rep, 1));
%                 v1(j)=var(observations(j,1:rep_cur(j)+add_rep, 1))/(rep_cur(j)+add_rep) + epsi;
%                 y2(j)=mean(observations(j,1:rep_cur(j)+add_rep, 2));
%                 v2(j)=var(observations(j,1:rep_cur(j)+add_rep, 2))/(rep_cur(j)+add_rep) + epsi;
%                 rep_cur(j) = rep_cur(j) + add_rep;
%             end
%         end
%         %ymean = [ymean; y]; yvar = [yvar; v];
%         
%         %%Fit the MNEK model with new data
%         b = ones(counter,1);
%         model1 = MNEK_model_kd(x(1:counter,:),y1(1:counter),v1(1:counter),0,2);
%         model2 = MNEK_model_kd(x(1:counter,:),y2(1:counter),v2(1:counter),0,2);
%         %%Make the prediction
%         y1_pred = MNEK_predict(model1,x_axis,0);
%         
%         %%Update Statistics
%         [yy1, yy2, xx] = paretoSearch(y1, y2, x, counter);
%         zz = [yy1 yy2];
%         
%         
%         [yStarE_i(i,kk), xStarInd] = min(y1_pred);
%         xStarE_i(i,:,kk)=x_axis(xStarInd,:);
%         z = xStarE_i;
%         
%     end
%     
%     if(stopCritMET*stopCriterionINUSE==0 && usedBudget(kk)<T)
%         i = i + 1;
%     end
%     
%     
end
% 
% 
% 
% paretoSize = size(xx,1);
% safe_solset_x = zeros(1,4);
% safe_sol_y1 = 0;
% safe_sol_y2 = 0;
% if min(yy1) < safety
%     safe_soln_present = 1;
% else
%     safe_soln_present = 0;
% end
% if safe_soln_present ==1
%     for i = 1:paretoSize
%         if yy1(i) < safety
%             safe_solset_x = [safe_solset_x ; xx(i, :)];
%             safe_sol_y1 = [safe_sol_y1; yy1(i)];
%             safe_sol_y2 = [safe_sol_y2; yy2(i)];
%         end
%     end
%     safe_solset_x(1,:) = [];
%     safe_sol_y1(1,:) = [];
%     safe_sol_y2(1,:) = [];
%     [minDistp, solutionIdx] = min(safe_sol_y2);
%     solution = safe_solset_x(solutionIdx, :);
%     minProbp = safe_sol_y1(solutionIdx);
% else
%     [minProbp, solutionIdx] = min(yy1);
%     solution = xx(solutionIdx, :);
%     minDistp = yy2(solutionIdx);
% end
% 
% angleMax = 75; angleMin = 0; speedMin = 1; speedMax = 20;
% zargp = zeros(1,4);
% zargp(1,1) = solution(1) * (angleMax - angleMin) + angleMin;
% zargp(1,2) = solution(2) * (angleMax - angleMin) + angleMin;
% zargp(1,3) = solution(3) * (speedMax - speedMin) + speedMin;
% zargp(1,4) = solution(4) * (speedMax - speedMin) + speedMin;
% %zzarg = [zzarg; zarg];
% %end