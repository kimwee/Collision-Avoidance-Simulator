function [ei_0 s1_0] = MOEIcalc_MNEK_kd(x_0,x,M_model1, M_model2,curr_best) % EI calculator

cov_design1 = zeros(size(x,1));
cov_design2 = zeros(size(x,1));
counter = size(curr_best, 1);%to note number of solutions in pareto set
k=size(x,1);
d=size(x,2);
D_X = zeros(k, k, d);
tempD_X = zeros(k*k,d);
for h = 1:d
    hh=1;
    for ll=1:k
        for l = 1:k 
            tempD_X(hh,h) = (x(ll,h) - x(l,h)).^2;
            D_X(ll,l,h) = (x(ll,h) - x(l,h)).^2;
            hh=hh+1;
        end
    end
end

distElem=zeros(1,2);
hh=1;
for j = 1:size(x,1)
    for k = 1:size(x,1)
        distElem=tempD_X(hh,:);
        cov_design1(j,k) = M_model1.sigma_z*exp(-distElem*M_model1.theta)+0.0001;
        cov_design2(j,k) = M_model2.sigma_z*exp(-distElem*M_model2.theta)+0.0001;
        hh=hh+1;
    end
end

b_0 = ones(size(x_0,1),1);
y1_0 = MNEK_predict(M_model1,x_0,0,inv(cov_design1)); 
s1_0 = MNEKmse_kd(x_0,x,M_model1.sigma_z,M_model1.theta,inv(cov_design1));
y2_0 = MNEK_predict(M_model2,x_0,0,inv(cov_design2)); 
s2_0 = MNEKmse_kd(x_0,x,M_model2.sigma_z,M_model2.theta,inv(cov_design2));
counts = size(x_0,1);
ei_0 = zeros(size(x_0,1),1);
pi_0 = zeros(size(x_0,1),1);
ei_Temp = zeros(counter,1);
y1_augtemp = zeros(counter,1);
y2_augtemp = zeros(counter,1);
pi_Temp = zeros(counter,1);
y1_aug = zeros(size(x,1),1);
y2_aug = zeros(size(x,1),1);
s1_0 = real(sqrt(s1_0));
s2_0 = real(sqrt(s2_0));
curr_best = sortrows(curr_best,1);
for i = 1:counts
    if s1_0(i)>0
        for j = 2:counter
        norm1_curr  =  normcdf((curr_best(j,1)-y1_0(i))/s1_0(i),0,1);
        norm1_prev  =  normcdf((curr_best(j-1,1)-y1_0(i))/s1_0(i),0,1);
        norm2_curr  =  normcdf((curr_best(j,2)-y2_0(i))/s2_0(i),0,1);
        norm2_prev  =  normcdf((curr_best(j-1,2)-y2_0(i))/s2_0(i),0,1);
        nor1_curr   =  normpdf((curr_best(j,1)-y1_0(i))/s1_0(i),0,1);
        nor1_prev   =  normpdf((curr_best(j-1,1)-y1_0(i))/s1_0(i),0,1);
        nor2_curr   =  normpdf((curr_best(j,2)-y2_0(i))/s2_0(i),0,1);
        nor2_prev   =  normpdf((curr_best(j-1,2)-y2_0(i))/s2_0(i),0,1);
        pi_Temp(j) = (norm1_curr -norm1_prev)*norm2_curr; %+(norm1_curr -norm1_prev)*norm2_curr);
        y1_augtemp = ((y1_0(i) * norm1_curr - s1_0(i)*nor1_curr) - (y1_0(i)*norm1_prev - s1_0(i)*nor1_prev))*norm2_curr;%0.5*(norm2_curr+norm2_prev);
        y2_augtemp = ((y2_0(i) * norm2_curr - s2_0(i)*nor2_curr) - (y2_0(i)*norm2_prev - s2_0(i)*nor2_prev))*norm1_curr;%0.5*(norm1_curr+norm1_prev);
        end
        pi_0(i) = sum(pi_Temp)+normcdf((curr_best(1,1)-y1_0(i))/s1_0(i),0,1)+(1-normcdf((curr_best(counter,1)-y1_0(i))/s1_0(i),0,1))*normcdf((curr_best(counter,2)-y2_0(i))/s2_0(i),0,1);
        if isnan(pi_0(i))
            pi_0(i) = 0;
        end
        if pi_0(i) < 1*10^-4
            ei_0(i) = pi_0(i);
        else
            y1_aug = (sum(y1_augtemp) + y1_0(i)*normcdf((curr_best(1,1)-y1_0(i))/s1_0(i),0,1) - s1_0(i)*normpdf((curr_best(1,1)-y1_0(i))/s1_0(i),0,1) + ...
            (y1_0(i)*normcdf((curr_best(counter,1)-y1_0(i))/s1_0(i),0,1) +s1_0(i)*normpdf((curr_best(counter,1)-y1_0(i))/s1_0(i),0,1))*normcdf((curr_best(counter,2)-y2_0(i))/s2_0(i),0,1))/pi_0(i);
            y2_aug = (sum(y2_augtemp) + y2_0(i)*normcdf((curr_best(1,2)-y2_0(i))/s2_0(i),0,1) - s2_0(i)*normpdf((curr_best(1,2)-y2_0(i))/s2_0(i),0,1) + ...
            (y2_0(i)*normcdf((curr_best(counter,2)-y2_0(i))/s2_0(i),0,1) +s2_0(i)*normpdf((curr_best(counter,2)-y2_0(i))/s2_0(i),0,1))*normcdf((curr_best(counter,1)-y1_0(i))/s1_0(i),0,1))/pi_0(i);
            minDist = findClosestPt([y1_aug y2_aug],curr_best);
            ei_0(i) = pi_0(i)* minDist;
        end
    end
    if s1_0(i)<=0
        pi_0(i)=0;
        ei_0(i)=0;
    end
end
end
