function [index varOP] = MOEI_MNEK_kd(x_0,x,M_model1, M_model2,curr_best, y, u) % EI calculator

cov_design1 = zeros(size(x,1));
cov_design2 = zeros(size(x,1));
counter = size(curr_best, 1);%to note number of solutions in pareto set
k=size(x,1);
d=size(x,2);
D_X = zeros(k, k, d);
tempD_X = zeros(k*k,d);
for h = 1:d
    hh=1;
    for ll=1:k
        for l = 1:k 
            tempD_X(hh,h) = (x(ll,h) - x(l,h)).^2;
            D_X(ll,l,h) = (x(ll,h) - x(l,h)).^2;
            hh=hh+1;
        end
    end
end

distElem=zeros(1,2);
hh=1;
for j = 1:size(x,1)
    for k = 1:size(x,1)
        distElem=tempD_X(hh,:);
        cov_design1(j,k) = M_model1.sigma_z*exp(-distElem*M_model1.theta)+0.0001;
        cov_design2(j,k) = M_model2.sigma_z*exp(-distElem*M_model2.theta)+0.0001;
        hh=hh+1;
    end
end

b_0 = ones(size(x_0,1),1);
y1_0 = MNEK_predict(M_model1,x_0,0,inv(cov_design1)); 
s1_0 = MNEKmse_kd(x_0,x,M_model1.sigma_z,M_model1.theta,inv(cov_design1));
y2_0 = MNEK_predict(M_model2,x_0,0,inv(cov_design2)); 
s2_0 = MNEKmse_kd(x_0,x,M_model2.sigma_z,M_model2.theta,inv(cov_design2));

counts = size(x_0,1);
ei_0 = zeros(size(x_0,1),1);
pi_0 = zeros(size(x_0,1),1);

pi_Temp = zeros(counter,1);
y1_aug = zeros(size(x,1),1);
y2_aug = zeros(size(x,1),1);
s1_0 = real(sqrt(s1_0));
s2_0 = real(sqrt(s2_0));

curr_best = sortrows(curr_best,1);
fmin = zeros(1,size(curr_best,2));
fmax = zeros(1,size(curr_best,2));
fmin(1,1) = min(y1_0);
fmin(1,2) = min(y2_0);
fmax(1,1) = max(y1_0);
fmax(1,2) = max(y2_0);
% for i = 1:2%i is number of objectives
%         fmin(i) = min(curr_best(:,i));
%         fmax(i) = max(curr_best(:,i));
% end
for i = 1:counts
    if s1_0(i)>0
        for j = 2:counter
        norm1_curr  =  normcdf((curr_best(j,1)-y1_0(i))/s1_0(i),0,1);
        norm1_prev  =  normcdf((curr_best(j-1,1)-y1_0(i))/s1_0(i),0,1);
        norm2_curr  =  normcdf((curr_best(j,2)-y2_0(i))/s2_0(i),0,1);

        pi_Temp(j) = (norm1_curr -norm1_prev)*norm2_curr; 
        end
        pi_0(i) = sum(pi_Temp)+normcdf((curr_best(1,1)-y1_0(i))/s1_0(i),0,1)+(1-normcdf((curr_best(counter,1)-y1_0(i))/s1_0(i),0,1))*normcdf((curr_best(counter,2)-y2_0(i))/s2_0(i),0,1);
        %pi_0(i) = 1;
        if isnan(pi_0(i))
            pi_0(i) = 0;
        end
        if pi_0(i) < 1*10^-4
            ei_0(i) = pi_0(i);
        else
            minDist = findClosestPt([y1_0(i) y2_0(i)], y, fmin, fmax);
            ei_0(i) = pi_0(i)* minDist;
        end
    end
    if s1_0(i)<=0
        pi_0(i)=0;
        ei_0(i)=0;
    end
end

ee = ei_0;
[EImax, idx] = max(ei_0);
y = [y; [y1_0(idx) y2_0(idx)]];%add selected point to total set
index = idx;
%assign first point

%u = 4; %u denotes number of points
for pt_Count = 2:u
    
    for i = 1:counts
        minDist = findClosestPt([y1_0(i) y2_0(i)], y, fmin, fmax);
        ei_0(i) = pi_0(i)* minDist;
    end
    ee = [ee ei_0];
    [EImax, idx] = max(ei_0);
    index = [index;idx];
    %assign 2nd point
     y = [y; [y1_0(idx) y2_0(idx)]];%add selected point to total set
end

% y = [y; [y1_0(idx) y2_0(idx)]];%add selected point to total set
% for i = 1:counts
%     minDist = findClosestPt([y1_0(i) y2_0(i)], y, fmin, fmax);
%     ei_0(i) = pi_0(i)* minDist;
% end
% ee = [ee ei_0];
% [EImax, idx] = max(ei_0);
% index = [index;idx];
% %assign 3rd point
% 
% y = [y; [y1_0(idx) y2_0(idx)]];%add selected point to total set
% for i = 1:counts
%     minDist = findClosestPt([y1_0(i) y2_0(i)], y, fmin, fmax);
%     ei_0(i) = pi_0(i)* minDist;
% end
% ee = [ee ei_0];
% [EImax, idx] = max(ei_0);
% index = [index;idx];
% %assign 4th point

if std(pi_0) == 0
    index = (randperm(counts, u)).';
end

if isnan(sum(s1_0)) || (sum(s1_0))==0
    varOP = s1_0;
else
    if isnan(sum(s2_0)) || (sum(s2_0)) == 0
        varOP = s2_0;
    else
        varOP = s1_0;
    end
    
end

end
