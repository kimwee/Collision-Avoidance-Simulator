% runs the two stage algorithm for a one-dimensional minimization example;

clear all; close all; clc

%% function under analysis

%function is defined in the file f.m;
%calls to this function can be replaced with calls to the simulation model
fhandle = @f; % test function handle;

%dimension of the function
d=1;

x_axis = (0.001:0.001:1)'; % vector of partitions for x axis

% Generating the true function for comparison purposes
y_true=zeros(size(x_axis,1),1);
for j = 1:size(x_axis,1)
    y_true(j)=fhandle(x_axis(j));
end

x_0 = [0.7057; 0.2008; 0.9689; 0.0850; 0.5924; 0.3923]; % initial design points vector

xStar=0.7460;
yStar = -11.4510;

MAXIMUM=2500;
maxDIST = 1;


%% --------------------- user defined parameters-------------------------
q1=1.0;
deltaNoise=1.0;%use this to linearly increase the noise level
%Noise function ATT'N REPLACE IT WITH THE DESIRED NOISE FUNCTION
noise_f = @(x) deltaNoise*q1*(abs(x)+0.0000001); %%standard deviation

a=0.01;%tolerance lavel for the stopping criterion
quadratic=0; %set to 1 for a quadratic increase in the number of replications
stopCriterionINUSE=0;%put 0 if you want to bring the optimization without stopping criterion

stopCritMET=0;%%control on the stopping criterion

%single observation
MAXITERCROSS=1; %maximum number of cross validation iteration you want to perform
alpha = 1.25; % single sided sigma limit for cross validation

%sample size of initial observations
n_0 = 6; % number of initial design points

deltaB=0;
delta=0;%Used to increase B at each iteration

%eTSSO: budgetIncrease =1; TSSO: budgetIncrease =0
budgetIncrease=0;
macroReplications=1; %number of macro-replications

%%
%%-------------------------------------------------------------------------
%%--------------------------------ETSSO------------------------------------
%%-------------------------------------------------------------------------

state = 12220;
randn('state',state);

%variables used to collect statistics
y_res=zeros(macroReplications,7+d);
ystarDiff=zeros(macroReplications,1);
xstarDiff=zeros(macroReplications,d);
xstarDiffTOT=zeros(macroReplications,1);
stopCritMETvec=zeros(macroReplications,1);
solutionperRep=zeros(macroReplications,1);
usedBudget=zeros(macroReplications,1);
bestX=zeros(macroReplications,d);
Kstar=zeros(macroReplications,1);
BestBudget=zeros(macroReplications,1);

B_n0_setting = 10; % number of replications available for the initial fit;
B_setting = 10;
T_setting = 1000;
r_min_setting = 10;
i_max = floor((T_setting-B_n0_setting*n_0)/B_setting);
Bit=zeros(macroReplications,i_max);

%% start of macro-replication
for kk=1:macroReplications
    r_min=r_min_setting;% minimum number of replications needed for one
    B=B_setting;
    B_n0 = B_n0_setting; % number of replications available for the initial fit;
    T = T_setting; % total number of replications in each macro-replication
    
    %extrinsic and intrinsic variance in each iteration
    ev_it = zeros(i_max,macroReplications);
    iv_it = zeros(i_max,macroReplications);
    
    observations=zeros(MAXIMUM,T);
    % for cross-validation
    iterCROSS=1; %no. of cross-validation
    cross_OK=1;
    
    %% initial fit
    %Start adding points until cross validate the model or max number of cv
    %iteration's been performed
    while (cross_OK>0)
        %generate the DOE
        x=x_0;
        y = zeros(MAXIMUM,1); % sample mean vector
        v = zeros(MAXIMUM,1); % sample variance vector
        rep_cur = zeros(MAXIMUM,1); % replications vector
        counter = n_0; % current number of design points
        
        %Obtaining observations for initial design
        for j = 1:counter
            T_D = normrnd(fhandle(x(j)),noise_f(x(j)),B_n0,1);
            y(j) = mean(T_D);
            v(j) = var(T_D)/B_n0;
            rep_cur(j) = B_n0;
            for k=1:B_n0
                observations(j,k)=T_D(k,1);
            end
        end
        
        cross_val = cv_1d(x(1:counter),y(1:counter),v(1:counter),alpha); % cross validation
        
        if (sum(cross_val)==0 || iterCROSS>=MAXITERCROSS)
            cross_OK=0;
            'cross-validation passed'
        else
            iterCROSS=iterCROSS+1;
            B_n0=B_n0+10;
            B=B_n0;
            T=T+10*n_0;
        end
    end
    
    b = ones(counter,1);
    
    %use initial design points to fit the model
    model = MNEK_model(x(1:counter),y(1:counter),v(1:counter),0,2);
    
    
    [bestY bestXInd] = min(y(1:counter));
    bestXtemp = x(bestXInd,:);
    
    
    % compute distance from the optimum
    myDistance=abs(bestXtemp-xStar);
    distanceTemp=myDistance;
    distanceStar=min(distanceTemp);
    
    Kstartemp=n_0;
    
    %% Performing the sequential two-stage algorithm
    i=1;
    % set the  initial B
    if kk > 1 && deltaB>0 
        B=B+deltaB;
    elseif budgetIncrease==1 
        %perform the initialization for the case of budget increase
        B=r_min;
    end
    i_max = floor((T-B_n0*n_0)/B);
    
    %%Initialize vectors
    ydiffIteration=zeros(i_max,1);
    xdiffIteration=zeros(i_max,d);
    xdiffStarIteration=zeros(i_max,d);
    ydiffStarIteration=zeros(i_max,1);
    SqrxdiffStarIteration=zeros(i_max,d);
    %Bit: Budget in each iteration
    Bit=zeros(i_max,1);
    
    BestBudgetTemp=T-B_n0*n_0;
    r_A = zeros(i_max,1); % allocation stage budget
    r_S = zeros(i_max,1); % sampling stage budget
    if (i_max+n_0) > MAXIMUM
        'warning: total design points exceeds maximum'
        exit
    end
    %T is updated to be the remaining budget after initial fit
    T=T-n_0*B_n0;
    stopCritMET=0;
    
    %% new budget rule, change B in each iteration
    while(usedBudget(kk)<T && stopCritMET*stopCriterionINUSE==0)
        if (i == 1)
            r_A(i) = min(floor((B-r_min)/i_max),T);
        else
            if delta>0 
                if quadratic==1
                    if T-usedBudget(kk) > B+delta*(i).^2
                        B = B+delta*(i).^2;
                        i_max=floor((T-usedBudget(kk)-B)/min(B,T-usedBudget(kk)-B));
                    else
                        B = T-usedBudget(kk);
                        i_max=1;
                    end
                else
                    if T-usedBudget(kk) > B+delta*(i)
                        B = B+delta*(i);
                        i_max=floor((T-usedBudget(kk)-B)/min(B,T-usedBudget(kk)-B));
                    else
                        B = T-usedBudget(kk);
                        i_max=1;
                    end
                end
            end  
        end
        
        curr_best = inf;
        for j = 1:counter
            if(y(j) < curr_best)
                curr_best = y(j);
            end
        end
        
        [EI_values SDextrinsic] = EIcalc_MNEK_kd(x_axis,x(1:counter),model,curr_best);
        [maxEIk index] = max(EI_values);
        
        %compute the terms you need for the new budget allocation rule
        
        [BESTY index1] = min(y(1:counter));
        [WORSTY index2] = max(y(1:counter));
        v_BARint = v(index1); %intrinsic variance at current best pt
        maxV_barInt=max(v(1:counter));%maximum intrinsic variance among all sampled pts
        v_BARext = SDextrinsic(index).^2;
        
        %compute the r_A(i)
        if i>1 && delta==0 && budgetIncrease==0
            % Quan's rule
            r_A(i) = r_A(i-1) + min(floor((B-r_min)/i_max),T-(i-1)*B);
        elseif i>1 && (delta>0 || budgetIncrease==1)
            r_A(i) = B-r_min;
        end
        
        if (B >= r_min && budgetIncrease==1) || (T-usedBudget(kk)-r_A(i) > 0 && budgetIncrease==0)
            if delta==0 && budgetIncrease==0
                r_S(i) = B - r_A(i);
            elseif (delta>0 || budgetIncrease==1)
                r_S(i) = r_min;
            end
            
            if stopCritMET*stopCriterionINUSE==0
                %% search stage
                x_new = x_axis(index);
                T_X = normrnd(fhandle(x_new),noise_f(x_new),r_S(i),1);
                y_new = mean(T_X);
                v_new = var(T_X)/r_S(i);
                counter = counter + 1;
                x(counter) = x_new;
                y(counter) = y_new;
                v(counter) = v_new;
                rep_cur(counter) = r_S(i);
                for k=1:r_S(i)
                    observations(counter,k)=T_X(k,1);
                end
            end
        end
        
        if(stopCritMET*stopCriterionINUSE==0)
            % compute the budget needed based on the indications from OCBA
            OCBA_output_1 = OCBA(rep_cur(1:counter),y(1:counter),v(1:counter),r_min);
            added_rep_OCBA = OCBA_output_1-rep_cur(1:counter,1);
            [ocbaPoint indexOCBA]=max(added_rep_OCBA);
            if i==1
                remaining_budget=T-usedBudget(kk);
                ev_it(i,kk) = v_BARext;
                iv_it(i,kk) = v_BARint;
            end
            
            if i>1 && budgetIncrease==1
                var_ref=v(index1);
                %nat: natural
                nat_increase=floor(B + B*(var_ref*10000000/(v_BARext*10000000+var_ref*10000000)));
                
                applied_increase = nat_increase;
                
                B = min(applied_increase,T-usedBudget(kk));
                if(B<0)
                    B=B;
                end
                if(T-usedBudget(kk)-B<=B)
                    B=T-usedBudget(kk);
                    remaining_budget=0;
                else
                    remaining_budget=T-usedBudget(kk)-B;
                end
                ev_it(i,kk) = v_BARext;
                iv_it(i,kk) = var_ref;
            end
            Bit(kk,i)=B;
            
            if remaining_budget>0 && budgetIncrease==1
                i_max=floor((T-usedBudget(kk)-B)/min(B,T-usedBudget(kk)-B));
            elseif remaining_budget==0 && budgetIncrease==1
                i_max=1;
            end   
            
            %% Evaluation Procedure
            if budgetIncrease==1
                r_A(i)=B-r_S(i);
            end
            
            usedBudget(kk)=usedBudget(kk)+r_S(i)+r_A(i);
            if T-usedBudget(kk)<B
                r_A(i)=r_A(i)+T-usedBudget(kk);
                usedBudget(kk)=T;
            end
            
            OCBA_output = OCBA(rep_cur(1:counter),y(1:counter),v(1:counter),r_A(i));
            
            for j = 1:counter
                if (OCBA_output(j) > rep_cur(j))
                    add_rep = OCBA_output(j) - rep_cur(j);
                    
                    T_D = normrnd(fhandle(x(j)),noise_f(x(j,1)),add_rep,1);
                    for k=rep_cur(j)+1:rep_cur(j)+add_rep
                        observations(j,k)=T_D(k-rep_cur(j),1);
                    end
                    y(j)=mean(observations(j,1:rep_cur(j)+add_rep));
                    v(j)=var(observations(j,1:rep_cur(j)+add_rep))/(rep_cur(j)+add_rep);
                    rep_cur(j) = rep_cur(j) + add_rep;
                end
            end
            
            %Fit the MNEK model with new data
            b = ones(counter,1);
            model = MNEK_model(x(1:counter),y(1:counter),v(1:counter),0,2);
            
            %Make the prediction
            y_pred = MNEK_predict(model,x_axis,0);
            
            %% Update Statistics
            [yStarE_i xStarInd] = min(y_pred);
            xStarE_i=x_axis(xStarInd);
            
            if i>1
                ydiffIteration(i,1)=abs(yStarE_i(1)-yStarE_iOld(1));
                xdiffIteration(i,:)=abs(xStarE_i(1)-xStarE_iOld(1));
                yStarE_iOld=yStarE_i(1);
                xStarE_iOld=xStarE_i(1);
            end
            
            if i==1
                ydiffIteration(i,1)=abs(yStarE_i(1));
                yStarE_iOld=yStarE_i(1);
                xdiffIteration(i)=abs(xStarE_i(1));
                xStarE_iOld=xStarE_i(1);
            end
            
            distxdiffStarIteration=maxDIST;
            for indCompStar=1:size(xStar,1)
                distxdiffStarIterationTemp=sqrt(sum((xStarE_i(1)-xStar(indCompStar)).^2));
                if(distxdiffStarIterationTemp<distxdiffStarIteration)
                    distxdiffStarIteration=distxdiffStarIterationTemp;
                    xdiffStarIteration(i)=xStarE_i(1)-xStar(indCompStar);
                    SqrxdiffStarIteration(i)=sqrt((xStarE_i(1)-xStar(indCompStar)).^2);
                end
            end
            ydiffStarIteration(i,:)=yStarE_i(1)-yStar(1,1);
            
            [yStarEwithin xStarIndwithin] = min(y_pred);
            xStarEwithin=x_axis(xStarIndwithin);
            for InddimMin=1:size(xStar,1)
                %%compute distance from the optimum
                myDistance=xStarEwithin-xStar(InddimMin,:);
                mySqDist=myDistance.^2;
                mySqrtSqDist=sqrt(sum(mySqDist));
                distanceTemp(InddimMin)=mySqrtSqDist;
            end
            distanceTempStar=min(distanceTemp);
            if distanceTempStar<distanceStar
                bestXtemp=xStarEwithin;
                Kstartemp=i;
                BestBudgetTemp=usedBudget(kk);
                distanceStar=distanceTempStar;
            end
        end
        
        if(stopCritMET*stopCriterionINUSE==0 && usedBudget(kk)<T)
            i = i + 1;
        end
    end
    
    %% if all budgets are used in initial fit
    if(i==1)
        %Make the prediction
        y_pred = MNEK_predict(model,x_axis,0);
        
        %Update Statistics
        [yStarE_i xStarInd] = min(y_pred);
        xStarE_i=x_axis(xStarInd);
        
        if i>1
            ydiffIteration(i,1)=abs(yStarE_i(1)-yStarE_iOld(1));
            xdiffIteration(i,:)=abs(xStarE_i(1)-xStarE_iOld(1));
            yStarE_iOld=yStarE_i(1);
            xStarE_iOld=xStarE_i(1);
        end
        
        if i==1
            ydiffIteration(i,1)=abs(yStarE_i(1));
            yStarE_iOld=yStarE_i(1);
            xdiffIteration(i)=abs(xStarE_i(1));
            xStarE_iOld=xStarE_i(1);
        end
        
        distxdiffStarIteration=maxDIST;
        for indCompStar=1:size(xStar,1)
            distxdiffStarIterationTemp=sqrt(sum((xStarE_i(1)-xStar(indCompStar)).^2));
            if(distxdiffStarIterationTemp<distxdiffStarIteration)
                distxdiffStarIteration=distxdiffStarIterationTemp;
                xdiffStarIteration(i)=xStarE_i(1)-xStar(indCompStar);
                SqrxdiffStarIteration(i)=sqrt((xStarE_i(1)-xStar(indCompStar)).^2);
            end
        end
        ydiffStarIteration(i,:)=yStarE_i(1)-yStar(1,1);
        
        [yStarEwithin xStarIndwithin] = min(y_pred);
        xStarEwithin=x_axis(xStarIndwithin);
        for InddimMin=1:size(xStar,1)
            %compute distance from the optimum
            myDistance=xStarEwithin-xStar(InddimMin,:);
            mySqDist=myDistance.^2;
            mySqrtSqDist=sqrt(sum(mySqDist));
            distanceTemp(InddimMin)=mySqrtSqDist;
        end
        distanceTempStar=min(distanceTemp);
        if distanceTempStar<distanceStar
            bestXtemp=xStarEwithin;
            Kstartemp=i;
            BestBudgetTemp=usedBudget(kk);
            distanceStar=distanceTempStar;
        end
    end
    
    %% finishing macro replication
    x_axIter=zeros(size(ydiffIteration,1),1);
    x_axIterSeq=zeros(size(ydiffStarIteration,1),1);
    for iterInd=1:size(ydiffIteration,1)
        x_axIter(iterInd,1)=iterInd;
    end
    for iterInd=1:size(ydiffStarIteration,1)
        x_axIterSeq(iterInd,1)=iterInd;
    end
    if i<i_max
        for iterInd=i+1:size(ydiffIteration,1)
            ydiffIteration(iterInd,1)=ydiffIteration(iterInd-1,1);
            xdiffIteration(iterInd,:)=xdiffIteration(iterInd-1,:);
            ydiffStarIteration(iterInd,1)=ydiffStarIteration(iterInd-1,1);
            xdiffStarIteration(iterInd,:)=xdiffStarIteration(iterInd-1,:);
        end
    end
 
    bestX(kk,:)=bestXtemp;
    Kstar(kk)=Kstartemp;
    BestBudget(kk)=BestBudgetTemp;
    [yStarE xStarInd] = min(y_pred);
    xStarE=x_axis(xStarInd,:);
    ystarDiff(kk)=abs(yStarE(1)-yStar(1));
    distxdiffStarIteration=maxDIST;
    for indCompStar=1:size(xStar,1)
        distxdiffStarIterationTemp=sqrt(sum((xStarE(1,:)-xStar(indCompStar,:)).^2));
        if(distxdiffStarIterationTemp<distxdiffStarIteration)
            distxdiffStarIteration=distxdiffStarIterationTemp;
            xstarDiff(kk,:)=xStarE_i(1,:)-xStar(indCompStar,:);
            xstarDiffTOT(kk,1)=sqrt(sum((xStarE_i(1,:)-xStar(indCompStar,:)).^2));
        end
    end
    solutionperRep(kk)=kk;
    y_res(kk,:) = [solutionperRep(kk) usedBudget(kk) xstarDiff(kk) ystarDiff(kk) xstarDiffTOT(kk) Kstar(kk) BestBudget(kk) stopCritMETvec(kk)];
    
end



