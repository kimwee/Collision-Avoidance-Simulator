function y = fkdp(x, n, bTime)
%Specify the two-dimensional test function in this file
%x represents input, n represents number of replications
%y is an array of size(x,1), containing the simulation output for each
%size(x,1) replications
%y = zeros(n, size(x,1));
    %for j = 1:n
        %for i = 1:size(x,1)
            
            %z = -5.*(1-(x(i,1)*2-1).^2).*(1-(x(i,2)*2-1).^2).*(4+(x(i,1)*2-1)).*(0.05.^((x(i,1)*2-1).^2)-0.05.^((x(i,2)*2-1).^2)).^2;        
            %y(j, i) = z + normrnd(0,0.0001);
        %end
        angleMax = 75; angleMin = 0; speedMin = 1; speedMax = 20;
        arg = zeros(4,1);
        arg(1) = x(1) * (angleMax - angleMin) + angleMin;
        arg(2) = x(2) * (angleMax - angleMin) + angleMin;
        arg(3) = x(3) * (speedMax - speedMin) + speedMin;
        arg(4) = x(4) * (speedMax - speedMin) + speedMin;
       
        commandLine = [num2str(n),  ' ', num2str(bTime), ' ', num2str(arg(1)), ' ', num2str(arg(2)), ' ', num2str(arg(3)),' ', num2str(arg(4))];
        
        fileIDO = fopen('matlabInput.txt', 'w');
        while fileIDO < 0
            fileIDO = fopen('matlabInput.txt', 'w');
        end
        fprintf(fileIDO, '%s', commandLine);
        
        fclose(fileIDO);
        
        sys = system('testFunc.exe');
        %if(sys != 0){system('testFunc.exe')};
        fileID = fopen('innerSimOP.txt','r');
        while fileID < 0
            fileID = fopen('innerSimOP.txt','r');
        end
        formatSpec = '%f %f';
        y = zeros(50,2);
        sizeA= [2 inf];
        y = fscanf(fileID,formatSpec, sizeA);
        z = mean(y(1,:));
        zz = mean(y(2,:));
        z1 = var(y(1,:))/n;
        zt = var(y(2,:))/n;
        y= transpose(y);
        fclose(fileID);
        %pass x(i) into system exe
        %
        %
    %end
end

