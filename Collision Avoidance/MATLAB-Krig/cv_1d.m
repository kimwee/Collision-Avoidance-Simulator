function f = cv_1d(x, r, v, alpha)

    n = size(x,1);
    f = zeros(n,1);
 
    for i = 1:n
        x_1 = [x(1:i-1);x(i+1:n)];
        r_1 = [r(1:i-1);r(i+1:n)];
        v_1 = [v(1:i-1);v(i+1:n)];
        model_1 = MNEK_model(x_1,r_1,v_1,0,2);
        y_1 = MNEK_predict(model_1,x(i),0);
        cov_design = zeros(n-1);
        for j = 1:n-1
            for k = 1:n-1
                cov_design(j,k) = model_1.sigma_z*exp(-model_1.theta*(x_1(j)-x_1(k))^2);
            end
        end
        cov_design = cov_design + diag(v_1);
        s_1 = MNEKmse_1d(x(i),x_1,model_1.sigma_z,model_1.theta,inv(cov_design));
        sigma = (r(i)-y_1)/s_1;
        if abs(sigma)>alpha
            f(i) = 1;
        end
    end
    
end