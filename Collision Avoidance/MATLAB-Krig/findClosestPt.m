function minDist = findClosestPt(pt, pareto_set, fmin, fmax)
    %y = zeros(1,2);
    dist = zeros(size(pareto_set,1),1);
    epsilRatio = 0.001;
    fdiff = zeros(1,size(pareto_set,2));
    for i = 1:size(pareto_set,2)%i is number of objectives
        fmin(i) = fmin(i) - abs(fmin(i))*epsilRatio;
        fmax(i) = fmax(i) + abs(fmax(i))*epsilRatio;
        fdiff(i) = fmax(i) - fmin(i);
    end
    fdiff2 = fdiff.*fdiff;
    total_dist = sqrt(sum(fdiff2));
    
    temp_pareto_set = sortrows(pareto_set,1);
    pareto_set = sortrows(pareto_set,1);
    tPt = zeros(1,size(pareto_set, 2));
    for i = 1:size(pareto_set, 2)
        if i==1
        tPt(1,i) = fmin(i);
        else
        tPt(1,i) = fmax(i);
        end
    end
    temp_pareto_set = [tPt ; temp_pareto_set];%add min point
    tPt = zeros(1,size(pareto_set, 2));
    for i = 1:size(pareto_set, 2)
        if i==1
        tPt(1,i) = fmax(i);
        else
        tPt(1,i) = fmin(i);
        end
    end
    temp_pareto_set = [temp_pareto_set; tPt];%add max point
    for i = 1:size(temp_pareto_set,1)
        dist(i) = sqrt((pt(1,1)-temp_pareto_set(i,1))^2 +(pt(1,2)-temp_pareto_set(i,2))^2);
    end
    [minDist, minIdx] = min(dist);
    minDist = minDist / total_dist;
    %y = set(minIdx,:);
end