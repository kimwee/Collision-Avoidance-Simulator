function y = MNEKmse_1d(X_0, X, sigma_z, Theta, Cov_Inverse)
% Calculate the MSE for the modified nugget effect model
% X_0 - all the possible design locations
% X - current design locations 
% sigma_z -the estimated process variance
% theta - the estimated sensitivity parameter
% cov_inverse - the inversed deterministic covariance matrix
% Modified Nugget Effect Kriging toolbox. By YIN Jun, QUAN Ning, NG Szu
% Hui, 2011-2012.

k = size(X,1);
temp = zeros(size(X_0,1),1);
onevector = ones(k,1);
Cov_Pred = zeros(k,1);

for c = 1:size(X_0,1)
    for i = 1:k
        Cov_Pred(i) = sigma_z.*exp(-Theta.*((X_0(c)-X(i)).^2));
    end
    temp(c) = sigma_z - Cov_Pred'*Cov_Inverse*Cov_Pred + ((1-onevector'*Cov_Inverse*Cov_Pred).^2)./(onevector'*Cov_Inverse*onevector);
end

y = temp;

end

