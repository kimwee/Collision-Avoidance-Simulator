function [rep_cur_new] = OCBA(rep_cur,y,v,reps)

[y_min loc] = min(y);
delta = y - y_min;
delta_s = delta.^2;
ratio = v ./ delta_s;

if (loc == 1)
    aspect_ratio = ratio ./ ratio(2);
else
    aspect_ratio = ratio ./ ratio(1);
end

aspect_ratio(loc) = 0;

aspect_ratio(loc) = sqrt(v(loc))*sqrt(sum(aspect_ratio.^2./v));

aspect_ratio = aspect_ratio./sum(aspect_ratio);

add_rep = round((reps).*aspect_ratio);
if (sum(add_rep)<reps)
    [min_v min_loc] = min(add_rep);
    add_rep(min_loc) = add_rep(min_loc) + (reps-sum(add_rep));
end

rep_cur_new = rep_cur + add_rep;

end