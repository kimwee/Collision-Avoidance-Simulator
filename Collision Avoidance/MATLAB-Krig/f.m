function y = f(x)
%Specify the one-dimensional test function in this file
    temp = zeros(size(x,1),1);
    for i = 1:size(x,1)
       temp(i) = 10*(0.2*(x(i)-0.02)+1)*cos(13*(x(i)-0.02));
    end
    y = temp;
end
