function y = unNorm(x, bool)
if(bool == 1)
    angleMax = 60; angleMin = 0; speedMin = 1; speedMax = 15;
else
    angleMax = 0; angleMin = -60; speedMin = 1; speedMax = 15;
end
y(:,1) = x(:,1) * (angleMax - angleMin) + angleMin;
y(:,2) = x(:,2) * (angleMax - angleMin) + angleMin;
y(:,3) = x(:,3) * (speedMax - speedMin) + speedMin;
y(:,4) = x(:,4) * (speedMax - speedMin) + speedMin;

end
% scatter3(x_0(:,1), x_0(:,2), y2_0);
% xlabel('theta 1');
% ylabel('theta 2');
% zlabel('Deviation');