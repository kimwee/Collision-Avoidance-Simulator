import os
import csv
import pandas as pd

year = 2014
month = 2
day = 17
hour = 12
minute = 0
sec = 0
startTime = pd.Timestamp(year,month,day,hour,minute,sec)
endTime = pd.Timestamp(year,month,day,hour + 3,minute+30,sec)
lonStart = 103.7741
lonEnd = 103.84517
latStart = 1.20016
latEnd = 1.24787

ans = pd.DataFrame({ 'Time' : range(1, 180 + 1 + 30 ,1), 'Count' : 0})
vessels = pd.DataFrame()
directory = os.path.join("..\\dynamic")
for root,dirs,files in os.walk(directory):
    for file in files:
        if file.endswith(".csv"):
            df = pd.read_csv(os.path.join("..\\dynamic\\", file))
            if 'timeStamp' in df:
                df['timeStamp'] =  pd.to_datetime(df['timeStamp'], format='%Y-%m-%dT', errors='ignore')
                df['longitude'] = df['longitude'] / 600000
                df['latitude'] = df['latitude'] / 600000
                df = df[(df['timeStamp'] >= startTime) & (df['timeStamp'] <= endTime)] 
                       # & (df['longitude'] >= lonStart) & (df['longitude'] <= lonEnd) 
                       # & (df['latitude'] >= latStart) & (df['latitude'] <= latEnd)]
                #print(df.dtypes)
                df = df.reset_index(drop=True)
                if not(df.empty):
                    vessels = vessels.append(df[df.index == 0])
                    #print(file)
                    #print(df[df.index == 0])
                    #print(vessels)
                    #print(df)
                time = 0
                indi = False
                for i, (index, row) in enumerate(df.iterrows()):
                    if ((row['longitude'] >= lonStart) and (row['longitude'] <= lonEnd) 
                        and (row['latitude'] >= latStart) and (row['latitude'] <= latEnd)):
                        #print(file + " has count")
                        if indi == False:
                            indi = True
                            timeSinceSt = pd.Timedelta(row['timeStamp'] - startTime).total_seconds()/60
                    
                        if i == len(df) - 1:
                            # process time
                            endT = pd.Timedelta(row['timeStamp'] - startTime).total_seconds()/60
                            ans.loc[((ans['Time'] >= timeSinceSt) & (ans['Time'] <= endT)),'Count'] += 1
                    else:
                        if indi == True:
                            #do smth
                            endT = pd.Timedelta(row['timeStamp'] - startTime).total_seconds()/60
                            ans.loc[((ans['Time'] >= timeSinceSt) & (ans['Time'] <= endT)),'Count'] += 1
                            indi = False
                            timeSinceSt = 0
                print(file)
                #break
    break
out1 = "..\\dynamic\\output\\output_" + str(year) + str(month) + str(day) + "_" + str(hour) + str(minute) + str(sec) + ".csv"
print(out1)
ans.to_csv(out1)
out2 = "..\\dynamic\\output\\vessels_" + str(year) + str(month) + str(day) + "_" + str(hour) + str(minute) + str(sec) + ".csv"
print(out2)
vessels.to_csv(out2)