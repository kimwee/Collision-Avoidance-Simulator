#pragma once
#include <math.h>
class pvector
{
private:

public:
	double x;
	double y;
	pvector operator+(const pvector&b);
	pvector operator-(const pvector&b);
	pvector operator*(const double b);
	pvector(double xx, double yy);
	double getDistance();
	double getx() const;
	double gety() const;
	~pvector();
	pvector();
};

