#include "stdafx.h"
#include "vesselGenerator.h"

vesselGenerator::vesselGenerator(vector<Vessel> vesselvect, int timeframe, bool type)
{
	timeFrame = timeframe;
	vesselVect.resize(timeFrame + 1);
	vesselVect[0] = vesselvect;
	if (type == true) {
		advanceRand();
	}
	else {
		advanceDeter();
	}
}
//vesselGenerator::vesselGenerator(ifstream &readFile)
//{
//	string read;
//	vector<Vessel> vesselVect;
//	while (getline(readFile, read)) //Read file
//	{
//		string aName;
//		double aLength, aLongitude, aLatitude, aCourse, aSpeed, time;
//		istringstream is(read);
//		is >> aName;
//		is >> aLength;
//		is >> aLongitude;
//		is >> aLatitude;
//		is >> aCourse;
//		is >> aSpeed;
//		is >> time;
//		Vessel aVessel(aName, aLength, aLongitude, aLatitude, aCourse, aSpeed, false);
//		if (time)
//		vesselVect.push_back(aVessel);
//	}
//}
vesselGenerator::vesselGenerator()
{
}

vesselGenerator::~vesselGenerator()
{
	vesselVect.clear();
}

void vesselGenerator::advanceRand() {
	vector<int> randTime;//tracks next random time for each random
	randTime.resize(vesselVect[0].size());
	for (int i = 0; i < randTime.size(); i++) {
		randTime[i] = distFunc::unif(generator);//distFunc::poissonGenerator(generator);
	}
	for (int t = 1; t < timeFrame + 1; t++) {
		vesselVect[t].resize(vesselVect[0].size());
		Concurrency::parallel_for(int(0), int(vesselVect[0].size()), [&](int i) {
			//for (int i = 0; i < ; i++){
			Vessel currVessel = vesselVect[t - 1][i];//copy vessel from previous time
			if (t >= randTime[i]) {//if equals time for random behavior, execute behavior change
				double speed = max(1, min(currVessel.getSpeed() + distFunc::randSpeedChange(generator), 15));
				double course = currVessel.getCourse() + distFunc::randCourseChange(generator);
				currVessel.setCourse(course);
				currVessel.setSpeed(speed);
				randTime[i] = randTime[i] + distFunc::unif(generator);//distFunc::poissonGenerator(generator);//update to next random time
			}
			currVessel.advanceTime(1);
			vesselVect[t][i] = currVessel;//assign currVessel into vector
		});
		//vesselVect[t] = vesselVect[t - 1];
		//pass vesselVect into ABM to generate next set of coordinates

	}
}
void vesselGenerator::advanceDeter() {
	for (int t = 1; t < timeFrame + 1; t++) {
		vesselVect[t].resize(vesselVect[0].size());
		for (int i = 0; i < vesselVect[0].size(); i++) {
			Vessel currVessel = vesselVect[t - 1][i];//copy vessel from previous time
			currVessel.advanceTime(1);
			vesselVect[t][i] = currVessel;//assign currVessel into vector
		}
	}
}

double vesselGenerator::max(double a, double b)
{
	return (a > b) ? a : b;
}

double vesselGenerator::min(double a, double b)
{
	return (a < b) ? a : b;
}
