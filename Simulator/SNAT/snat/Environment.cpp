#include "stdafx.h"
#include "Environment.h"


Environment::Environment()
{
}


Environment::~Environment()
{
}

void Environment::initialiseEnvironment(int argc, char *argv[])
{
	trajIPFileName = "..\\Data\\testFunctrajInput";
	itrajIPFileName = "..\\Data\\itrajInput";
	vesselIPFileName = "..\\Data\\vesselInput";


	time(&start_timer);
	generator = default_random_engine(SEEDNUM); //the seed helps to repeat the experiment

	int numTest(0);
	std::cout << "Number of arguments received: " << argc << std::endl;
	try {
		if (argc < 1) {
			errorMsg = "Invalid Arguments";
			throw errorMsg;
		}
		int numIteration(argc - 1);
		int countIteration(1);
		if (argc == 1)
			numIteration = argc;

		while (countIteration <= numIteration)
		{
			countIteration++;
			std::string fileTestName = "TestRun";
			if (argc == 1)
			{
				std::cout << "Input the desired Test Case: " << std::endl;
				std::cin >> numTest;
			}
			else {
				std::string numTestStr = argv[countIteration];
				numTest = atoi(numTestStr.c_str());
			}
			std::cout << "Test Number selected: " << numTest << std::endl;

			if (numTest <= 0 || numTest > 17) {
				errorMsg = "Invalid Test Number";
				throw errorMsg;
			}
			std::stringstream ss;
			ss << numTest;
			strAdd = ss.str();
			fileTestName += strAdd;
			fileTestName += ".txt";

			cout << "The fileTestName:" << fileTestName.c_str() << endl;

			//std::ifstream readFile(fileTestName, ios::binary | ios::ate);
			std::ifstream readFile(fileTestName);
			cout << "is open?" << readFile.is_open() << endl;
			cout << "end of file?" << readFile.eof() << endl;

			//cout << "size of the test file:" << fileTestName << " = " << readFile.tellg() << endl;

			std::string outFileNames;
			outFileNames = "OutConflicts";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outConflictF(outFileNames.c_str());
			outFileNames = "Collision";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outCollision(outFileNames.c_str());
			collisionOutput.open(outFileNames);
			outFileNames = "CPA";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outCPA(outFileNames.c_str());
			outFileNames = "Manoeuvre";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outManeuvre(outFileNames.c_str());
			outFileNames = "Infor";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outInfor(outFileNames.c_str());
			outFileNames = "Check";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outCheck(outFileNames.c_str());
			outFileNames = "Manoeuvre";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outManoeuvreF(outFileNames.c_str());
			outFileNames = "Randspeed";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outSpeedF(outFileNames.c_str());
			outFileNames = "RandCourse";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outCourseF(outFileNames.c_str());
			outFileNames = "OwnHistory";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outOwnHistoryF(outFileNames.c_str());
			outFileNames = "TargetHistory";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outTargetHistoryF(outFileNames.c_str());
			string folder = "..\\Data\\";

			string ownFileName = folder + "kownHist";//for CAP modified trajectory
			string vesselFileName = folder + "kvesselHist";//for vessel states
			string origOwnOutputFileName = folder + "korigOwn";//for initial reference trajectory
			string collisionFileName = folder + "kcollHist";//for collision output for traj with CAP
			string icollisionFileName = folder + "kicollHist";//for initial reference trajectory collisions
			string fitnessOutputFileName = folder + "kfitOutput";
			ownFileName += strAdd + ".txt";
			vesselFileName += strAdd + ".txt";
			collisionFileName += strAdd + ".txt";
			icollisionFileName += strAdd + ".txt";
			origOwnOutputFileName += strAdd + ".txt";
			fitnessOutputFileName += strAdd + ".txt";
			vesselOutput.open(vesselFileName);
			ownOutput.open(ownFileName);
			collOutput.open(collisionFileName);
			icollOutput.open(icollisionFileName);
			origOwnOutput.open(origOwnOutputFileName);
			fitnessOutput.open(fitnessOutputFileName);

			// Mover * realEnv = new Mover();
			// pathGenerator* CollisionAvd = new pathGenerator();

			string read; //Temp string to read line from txt

			//vector<Collision> collisionVector;
			//vector<Conflict> conflictVector;
			safeDis = 0;
			currTime = 0;
			lookAhdTime = 10;
			// original 24 * 60
			runTime = 180; // 24 hours
			MT = 15; // minitor time in minutes
			mt = 1; //increment time in minutes
			numIndex = 0;
			safeFactor = 2.0;

			while (!readFile.eof()) //Read file
			{
				getline(readFile, read);
				if (read[0] == 'V') {
					isValidation = true;
				}
				else if (read != "") {
					if (isValidation) {
						string aName;
						double aLength, aLongitude, aLatitude, aCourse, aSpeed;
						istringstream is(read);
						is >> aName;
						is >> aLength;
						is >> aLongitude;
						is >> aLatitude;
						is >> aCourse;
						is >> aSpeed;
						int t;
						is >> t;
						Vessel aVessel(aName, aLength, aLongitude, aLatitude, aCourse, aSpeed, false);
						aVessel.time = t;
						vesselVector.push_back(aVessel);
						//cout << "new vessel created from: " << read << endl;
						aVessel.printVessel();
					}
					else {
						string aName;
						double aLength, aLongitude, aLatitude, aCourse, aSpeed;
						istringstream is(read);
						is >> aName;
						is >> aLength;
						is >> aLongitude;
						is >> aLatitude;
						is >> aCourse;
						is >> aSpeed;
						Vessel aVessel(aName, aLength, aLongitude, aLatitude, aCourse, aSpeed, false);
						vesselVector.push_back(aVessel);
						//cout << "new vessel created from: " << read << endl;
						aVessel.printVessel();
					}

				}
			}

			if (vesselVector.size() == 0) {
				cout << "no vessel data read in, skip this run" << endl;
				break;
			}

			Vessel own = vesselVector[0];
			// vesselVector.erase(vesselVector.begin());
			vesselVector[0].isOwnVessel = true;
			myVessel = OwnVessel(own.getName(), own.getLength(), own.getLongitude(), own.getLatitude(), own.getCourse(), own.getSpeed(), own.getStatus());
			coord lonlat(own.getLongitude(), own.getLatitude(), own.getSpeed());//103.586, 1.02735, 15);
			ownState = ownState.genDetTrajectory(3, lonlat);
			ownState.vesselLength = 100;//own.getLength();
			initState = ownState;
			safeDis = ownState.vesselLength * 4;
			// TODO: update ownstate into myvessel
			myVessel.setTrajectoryPos(0);
			myVessel.setOwnTrajectory(ownState);
		}
		densityOutput.open("densityOutput.txt");
		conflictOutput.open("conflictOutput.txt");
	}
	catch (std::string error)
	{
		std::cout << "Exception occurred: " << error << std::endl;
	}

}

void Environment::initialiseVEnvironment(int argc, char * argv[])
{
	trajIPFileName = "..\\Data\\testFunctrajInput";
	itrajIPFileName = "..\\Data\\itrajInput";
	vesselIPFileName = "..\\Data\\vesselInput";


	time(&start_timer);
	std::default_random_engine generator(SEEDNUM); //the seed helps to repeat the experiment

	int numTest(0);
	std::cout << "Number of arguments received: " << argc << std::endl;
	try {
		if (argc < 1) {
			errorMsg = "Invalid Arguments";
			throw errorMsg;
		}
		int numIteration(argc - 1);
		int countIteration(1);
		if (argc == 1)
			numIteration = argc;

		while (countIteration <= numIteration)
		{
			countIteration++;
			std::string fileTestName = "TestRunV";
			if (argc == 1)
			{
				std::cout << "Input the desired Test Case: " << std::endl;
				std::cin >> numTest;
			}
			else {
				std::string numTestStr = argv[countIteration];
				numTest = atoi(numTestStr.c_str());
			}
			std::cout << "Test Number selected: " << numTest << std::endl;

			if (numTest <= 0 || numTest > 25) {
				errorMsg = "Invalid Test Number";
				throw errorMsg;
			}
			std::stringstream ss;
			ss << numTest;
			strAdd = ss.str();
			fileTestName += strAdd;
			fileTestName += ".txt";

			cout << "The fileTestName:" << fileTestName.c_str() << endl;

			//std::ifstream readFile(fileTestName, ios::binary | ios::ate);
			std::ifstream readFile(fileTestName);
			cout << "is open?" << readFile.is_open() << endl;
			cout << "end of file?" << readFile.eof() << endl;

			//cout << "size of the test file:" << fileTestName << " = " << readFile.tellg() << endl;

			std::string outFileNames;
			outFileNames = "OutConflicts";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outConflictF(outFileNames.c_str());
			outFileNames = "Collision";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outCollision(outFileNames.c_str());
			outFileNames = "CPA";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outCPA(outFileNames.c_str());
			outFileNames = "Manoeuvre";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outManeuvre(outFileNames.c_str());
			outFileNames = "Infor";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outInfor(outFileNames.c_str());
			outFileNames = "Check";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outCheck(outFileNames.c_str());
			outFileNames = "Manoeuvre";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outManoeuvreF(outFileNames.c_str());
			outFileNames = "Randspeed";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outSpeedF(outFileNames.c_str());
			outFileNames = "RandCourse";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outCourseF(outFileNames.c_str());
			outFileNames = "OwnHistory";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outOwnHistoryF(outFileNames.c_str());
			outFileNames = "TargetHistory";
			outFileNames += strAdd;
			outFileNames += ".txt";
			std::ofstream outTargetHistoryF(outFileNames.c_str());
			string folder = "..\\Data\\";

			string ownFileName = folder + "kownHist";//for CAP modified trajectory
			string vesselFileName = folder + "kvesselHist";//for vessel states
			string origOwnOutputFileName = folder + "korigOwn";//for initial reference trajectory
			string collisionFileName = folder + "kcollHist";//for collision output for traj with CAP
			string icollisionFileName = folder + "kicollHist";//for initial reference trajectory collisions
			string fitnessOutputFileName = folder + "kfitOutput";
			ownFileName += strAdd + ".txt";
			vesselFileName += strAdd + ".txt";
			collisionFileName += strAdd + ".txt";
			icollisionFileName += strAdd + ".txt";
			origOwnOutputFileName += strAdd + ".txt";
			fitnessOutputFileName += strAdd + ".txt";
			vesselOutput.open(vesselFileName);
			ownOutput.open(ownFileName);
			collOutput.open(collisionFileName);
			icollOutput.open(icollisionFileName);
			origOwnOutput.open(origOwnOutputFileName);
			fitnessOutput.open(fitnessOutputFileName);

			// Mover * realEnv = new Mover();
			// pathGenerator* CollisionAvd = new pathGenerator();

			string read; //Temp string to read line from txt

						 //vector<Collision> collisionVector;
						 //vector<Conflict> conflictVector;
			safeDis = 0;
			currTime = 0;
			lookAhdTime = 10;
			// original 24 * 60
			runTime = 180; // 24 hours
			MT = 15; // minitor time in minutes
			mt = 1; //increment time in minutes
			numIndex = 0;
			safeFactor = 2.0;

			while (!readFile.eof()) //Read file
			{
				getline(readFile, read);
				if (read != "") {
					string aName;
					double aLength, aLongitude, aLatitude, aCourse, aSpeed;
					istringstream is(read);
					is >> aName;
					is >> aLength;
					is >> aLongitude;
					is >> aLatitude;
					is >> aCourse;
					is >> aSpeed;
					int t;
					is >> t;
					Vessel aVessel(aName, aLength, aLongitude, aLatitude, aCourse, aSpeed, false);
					aVessel.time = t;
					vesselVector.push_back(aVessel);
					//cout << "new vessel created from: " << read << endl;
					aVessel.printVessel();
				}
			}

			if (vesselVector.size() == 0) {
				cout << "no vessel data read in, skip this run" << endl;
				break;
			}

			Vessel own = vesselVector[0];
			// vesselVector.erase(vesselVector.begin());
			vesselVector[0].isOwnVessel = true;
			myVessel = OwnVessel(own.getName(), own.getLength(), own.getLongitude(), own.getLatitude(), own.getCourse(), own.getSpeed(), own.getStatus());
			coord lonlat(own.getLongitude(), own.getLatitude(), own.getSpeed());//103.586, 1.02735, 15);
			ownState = ownState.genDetTrajectory(3, lonlat);
			ownState.vesselLength = 100;//own.getLength();
			initState = ownState;
			safeDis = ownState.vesselLength * 4;
			// TODO: update ownstate into myvessel
			myVessel.setTrajectoryPos(0);
			myVessel.setOwnTrajectory(ownState);
		}
		densityOutput.open("densityOutput.txt");
		conflictOutput.open("conflictOutput.txt");
	}
	catch (std::string error)
	{
		std::cout << "Exception occurred: " << error << std::endl;
	}

}

void Environment::assignPatterns()
{
	printTraject(ownState);
	/*Start the ABM for other vessels*/
	pattern_based_mover = PathMoverTrajectoryBased();

	pattern_based_mover.assignOwnVessel(myVessel);
	pattern_based_mover.clearAppendBasedFileStream(); // clear historical append based out file stream
	pattern_based_mover.prepareOutFileStreams(strAdd); // ofstream created
	pattern_based_mover.loadPatternRelatedData(); // load pattern data
	pattern_based_mover.prepareRandomEngine(); // prepare rand engine
											   //pattern_based_mover.assignInitialPatternsToVessels(vesselVector); // assign initial pattern
	pattern_based_mover.assignPatternsToVesselsNearest(vesselVector); // assign pattern according to read in vessel lat, lon and course
	pattern_based_mover.copyOwnPattern(myVessel, vesselVector[0]);
	for (int i = 0; i < vesselVector.size(); i++) {
		vesselVector[i].printPatternRelated();
	}
	pattern_based_mover.isValidation = isValidation;
}

void Environment::close()
{
	/*close stream to actually write to file */
	pattern_based_mover.closeOutFileStreams();
}

void Environment::runSimulation()
{
	vesselGenerator futureState;//futureState used to checkfuture under random walk conditions

	//vector<Vessel> tl, tr, br, bl;
	//double lon = myVessel.getLongitude(), lat = myVessel.getLatitude();
	//for (int i = 1; i < vesselVector.size(); i++) {
	//	if (vesselVector[i].getLatitude() - lat > 0) {
	//		if (vesselVector[i].getLongitude() - lon > 0) {
	//			tr.push_back(vesselVector[i]);
	//		}
	//		else {
	//			tl.push_back(vesselVector[i]);
	//		}
	//	}
	//	else {
	//		if (vesselVector[i].getLongitude() - lon > 0) {
	//			br.push_back(vesselVector[i]);
	//		}
	//		else {
	//			bl.push_back(vesselVector[i]);
	//		}
	//	}
	//}

	//pair<double, double> temp = PathMoverTrajectoryBased::XYToLatLon(lat, lon, 3704, 3704);
	//coord ctr(temp.second, temp.first);
	//temp = PathMoverTrajectoryBased::XYToLatLon(lat, lon, -3704, 3704);
	//coord ctl(temp.second, temp.first);
	//temp = PathMoverTrajectoryBased::XYToLatLon(lat, lon, 3704, -3704);
	//coord cbr(temp.second, temp.first);
	//temp = PathMoverTrajectoryBased::XYToLatLon(lat, lon, -3704, -3704);
	//coord cbl(temp.second, temp.first);

	//double x1 = 6.371 * cos(ctl.lat*PI / 180) * cos(ctl.lon*PI / 180);
	//double y1 = 6.371 * cos(ctl.lat*PI / 180) * sin(ctl.lon*PI / 180);
	//double x2 = 6.371 * cos(ctr.lat*PI / 180) * cos(ctr.lon*PI / 180);
	//double y2 = 6.371 * cos(ctr.lat*PI / 180) * sin(ctr.lon*PI / 180);
	//double x3 = 6.371 * cos(cbr.lat*PI / 180) * cos(cbr.lon*PI / 180);
	//double y3 = 6.371 * cos(cbr.lat*PI / 180) * sin(cbr.lon*PI / 180);
	//double x4 = 6.371 * cos(cbl.lat*PI / 180) * cos(cbl.lon*PI / 180);
	//double y4 = 6.371 * cos(cbl.lat*PI / 180) * sin(cbl.lon*PI / 180);
	//double original = (0.3704*0.3704) /10;

	//coord cd(lon, lat);
	//double dist = cpaCalc::getDistance(vesselVector[0], ctr);
	//for (int i = 0; i < tr.size(); i++) {
	//	if (cpaCalc::getDistance(tr[i], cd) < dist) {
	//		ctr = coord(tr[i].getLongitude(), tr[i].getLatitude());
	//		dist = cpaCalc::getDistance(tr[i], cd);
	//	}
	//}
	//dist = cpaCalc::getDistance(vesselVector[0], ctl);
	//for (int i = 0; i < tl.size(); i++) {
	//	if (cpaCalc::getDistance(tl[i], cd) < dist) {
	//		ctl = coord(tl[i].getLongitude(), tl[i].getLatitude());
	//		dist = cpaCalc::getDistance(tl[i], cd);
	//	}
	//}
	//dist = cpaCalc::getDistance(vesselVector[0], cbr);
	//for (int i = 0; i < br.size(); i++) {
	//	if (cpaCalc::getDistance(br[i], cd) < dist) {
	//		cbr = coord(br[i].getLongitude(), br[i].getLatitude());
	//		dist = cpaCalc::getDistance(br[i], cd);
	//	}
	//}
	//dist = cpaCalc::getDistance(vesselVector[0], cbl);
	//for (int i = 0; i < bl.size(); i++) {
	//	if (cpaCalc::getDistance(bl[i], cd) < dist) {
	//		cbl = coord(bl[i].getLongitude(), bl[i].getLatitude());
	//		dist = cpaCalc::getDistance(bl[i], cd);
	//	}
	//}

	//x1 = 6.371 * cos(ctl.lat*PI / 180) * cos(ctl.lon*PI / 180);
	//y1 = 6.371 * cos(ctl.lat*PI / 180) * sin(ctl.lon*PI / 180);
	//x2 = 6.371 * cos(ctr.lat*PI / 180) * cos(ctr.lon*PI / 180);
	//y2 = 6.371 * cos(ctr.lat*PI / 180) * sin(ctr.lon*PI / 180);
	//x3 = 6.371 * cos(cbr.lat*PI / 180) * cos(cbr.lon*PI / 180);
	//y3 = 6.371 * cos(cbr.lat*PI / 180) * sin(cbr.lon*PI / 180);
	//x4 = 6.371 * cos(cbl.lat*PI / 180) * cos(cbl.lon*PI / 180);
	//y4 = 6.371 * cos(cbl.lat*PI / 180) * sin(cbl.lon*PI / 180);

	//double area = abs(x1*y2 - y1*x2 + x2*y3 - y2*x3 + x3*y4 - y3*x4 + x4*y1 - y4*x1) / 2;
	//double ratio = area / original;
	//cout << ratio << area << original << endl;


	//int count = 0;
	//double lon = myVessel.getLongitude(), lat = myVessel.getLatitude();
	//coord cd(lon, lat);
	//for (int i = 1; i < vesselVector.size(); i++) {
	//	//if (cpaCalc::getDistance(vesselVector[i], cd) < 27780) {
	//	//	count++;
	//	//}
	//	count += cpaCalc::getDistance(vesselVector[i], cd);
	//}
	//count = count / (vesselVector.size() - 1);
	//cout << count << endl;

	if (isValidation) {
		vector<Vessel> all = vesselVector;
		vesselVector = vector<Vessel>();
		std::uniform_real_distribution<double> distribution(0.0, 1.0);
		for (int loopTime = 0; loopTime <= runTime; loopTime += mt) //Simulate for 3 hours
		{
			while (!all.empty() && all[0].time == loopTime) {
				vesselVector.push_back(all[0]);
				all.erase(all.begin());
			}
			coord ownCoord = ownState.getCoordAt(loopTime);
			coord ownICoord = initState.getCoordAt(loopTime);
			chkCollision(vesselVector, ownCoord, loopTime);
			cout << loopTime << " " << ownCoord.lon << " " << ownCoord.lat << " " << ownCoord.course << endl;
			printOwn(loopTime);
			printVessel(loopTime);

			if (loopTime % 1 == 0) {//check future every 1 mins

									//ABM required Code
				time(&start_timer);
				pattern_based_mover.advanceOwnVessel(mt, myVessel, loopTime);
				pattern_based_mover.advanceAllOtherVesselsPatternBasedWrapper(mt, vesselVector, loopTime);
				int i = 0;
				while (i < vesselVector.size()) {
					if (vesselVector[i].isEnd) {
						vesselVector.erase(vesselVector.begin() + i);
					}
					else if (vesselVector[i].isWait) {
						//int time = 1.0 + distribution(generator) * 90.0;
						int time = 5.0 + distribution(generator) * 180.0;
						//int time = getTriangular(distribution(generator));
						Vessel temp = vesselVector[i];
						temp.time = loopTime + time;
						temp.isWait = false;
						vesselVector.erase(vesselVector.begin() + i);
						int j = 0;
						while (j < all.size() && all[j].time < temp.time) {
							j++;
						}
						if (j == all.size()) {
							all.push_back(temp);
						}
						else {
							all.insert(all.begin() + j, temp);
						}
					}
					else {
						i++;
					}
				}
				if (myVessel.endPoint) { // at the end, copy other way
					pattern_based_mover.copyOwnPattern(myVessel, vesselVector[0]);
					myVessel.endPoint = false;
				}
				else {
					pattern_based_mover.copyOwnVessel(myVessel, vesselVector);
				}
				//ABM required Code

				//Generate deterministic lookahead
				/*			Vessel temp = vesselVector[0];
				vesselVector.erase(vesselVector.begin());
				futureState = vesselGenerator(vesselVector, lookAhdTime, false);
				//Generate own future coordinates
				// TODO: use myvessel instead
				vector<coord> ownFuture = myVessel.getOwnTrajectory().getCoordAhead(loopTime, lookAhdTime);
				//bool collision = chkFuture(futureState, ownFuture, t);

				bool collision = false;
				int checkcount = 0;
				collision = chkFuture(futureState, ownFuture, loopTime);
				cout << "checkcount: " << checkcount << endl;
				if (collision == true) {
				cout << "Collision Predicted!" << endl;
				//Call Optimizer
				myVessel.updateTrajectory();
				coord pt(myVessel.getLongitude(), myVessel.getLatitude(), myVessel.getCourse(), myVessel.getSpeed());
				myVessel.setOwnTrajectory(solver.getSolution(myVessel.getOwnTrajectory(), loopTime, initState, vesselVector, myVessel.getLength(), myVessel.getTrajectoryPos(), pt));
				// TODO: update myvessel instead of ownstate
				myVessel.updateTime(loopTime);
				}
				vesselVector.insert(vesselVector.begin(), temp); */
				time(&end_timer);
				cout << "simulation tick: " << loopTime << "/" << runTime
					<< ", time spent:" << difftime(end_timer, start_timer) << endl;
				// TODO: Advance own vessel + print to target history
			}

			updateDensity(loopTime, vesselVector);
			checkConflict(vesselVector, myVessel, loopTime);
		}
	}
	else {
		for (int loopTime = 0; loopTime <= runTime; loopTime += mt) //Simulate for 2 hours
		{
			coord ownCoord = ownState.getCoordAt(loopTime);
			coord ownICoord = initState.getCoordAt(loopTime);
			chkCollision(vesselVector, ownCoord, loopTime);
			cout << loopTime << " " << ownCoord.lon << " " << ownCoord.lat << " " << ownCoord.course << endl;
			printOwn(loopTime);
			printVessel(loopTime);

			if (loopTime % 1 == 0) {//check future every 1 mins

				//ABM required Code
				time(&start_timer);
				pattern_based_mover.advanceOwnVessel(mt, myVessel, loopTime);
				pattern_based_mover.advanceAllOtherVesselsPatternBasedWrapper(mt, vesselVector, loopTime);
				if (myVessel.endPoint) { // at the end, copy other way
					pattern_based_mover.copyOwnPattern(myVessel, vesselVector[0]);
					myVessel.endPoint = false;
				}
				else {
					pattern_based_mover.copyOwnVessel(myVessel, vesselVector);
				}
				//ABM required Code

				//Generate deterministic lookahead
				Vessel temp = vesselVector[0];
				vesselVector.erase(vesselVector.begin());
				futureState = vesselGenerator(vesselVector, lookAhdTime, false);
				//Generate own future coordinates
				// TODO: use myvessel instead
				vector<coord> ownFuture = myVessel.getOwnTrajectory().getCoordAhead(loopTime, lookAhdTime);
				//bool collision = chkFuture(futureState, ownFuture, t);

				bool collision = false;
				int checkcount = 0;
				collision = chkFuture(futureState, ownFuture, loopTime);
				cout << "checkcount: " << checkcount << endl;
				if (collision == true) {
					cout << "Collision Predicted!" << endl;
					//Call Optimizer
					myVessel.updateTrajectory();
					coord pt(myVessel.getLongitude(), myVessel.getLatitude(), myVessel.getCourse(), myVessel.getSpeed());
					myVessel.setOwnTrajectory(solver.getSolution(myVessel.getOwnTrajectory(), loopTime, initState, vesselVector, myVessel.getLength(), myVessel.getTrajectoryPos(), pt));
					// TODO: update myvessel instead of ownstate
					myVessel.updateTime(loopTime);
				}
				vesselVector.insert(vesselVector.begin(), temp);
				time(&end_timer);
				cout << "simulation tick: " << loopTime << "/" << runTime
					<< ", time spent:" << difftime(end_timer, start_timer) << endl;
				// TODO: Advance own vessel + print to target history
			}

			updateDensity(loopTime, vesselVector);
			checkConflict(vesselVector, myVessel, loopTime);
			checkCollision(vesselVector, myVessel, loopTime);
		}

	}



}

void Environment::runVSimulation()
{

	vesselGenerator futureState;//futureState used to checkfuture under random walk conditions
	vector<Vessel> all = vesselVector;
	vesselVector = vector<Vessel>();
	std::uniform_real_distribution<double> distribution(0.0, 1.0);
	for (int loopTime = 0; loopTime <= runTime; loopTime += mt) //Simulate for 3 hours
	{
		while (!all.empty() && all[0].time == loopTime) {
			vesselVector.push_back(all[0]);
			all.erase(all.begin());
		}
		coord ownCoord = ownState.getCoordAt(loopTime);
		coord ownICoord = initState.getCoordAt(loopTime);
		chkCollision(vesselVector, ownCoord, loopTime);
		cout << loopTime << " " << ownCoord.lon << " " << ownCoord.lat << " " << ownCoord.course << endl;
		printOwn(loopTime);
		printVessel(loopTime);

		if (loopTime % 1 == 0) {//check future every 1 mins

								//ABM required Code
			time(&start_timer);
			pattern_based_mover.advanceOwnVessel(mt, myVessel, loopTime);
			pattern_based_mover.advanceAllOtherVesselsPatternBasedWrapper(mt, vesselVector, loopTime);
			int i = 0;
			while (i < vesselVector.size()) {
				if (vesselVector[i].isEnd) {
					vesselVector.erase(vesselVector.begin() + i);
				}
				else if (vesselVector[i].isWait) {
					//int time = 1.0 + distribution(generator) * 90.0;
					int time = 5.0 + distribution(generator) * 180.0;
					//int time = getTriangular(distribution(generator));
					Vessel temp = vesselVector[i];
					temp.time = loopTime + time;
					temp.isWait = false;
					vesselVector.erase(vesselVector.begin() + i);
					int j = 0;
					while (j < all.size() && all[j].time < temp.time) {
						j++;
					}
					if (j == all.size()) {
						all.push_back(temp);
					}
					else {
						all.insert(all.begin() + j, temp);
					}
				}
				else {
					i++;
				}
			}
			if (myVessel.endPoint) { // at the end, copy other way
				pattern_based_mover.copyOwnPattern(myVessel, vesselVector[0]);
				myVessel.endPoint = false;
			}
			else {
				pattern_based_mover.copyOwnVessel(myVessel, vesselVector);
			}
			//ABM required Code

			//Generate deterministic lookahead
			/*			Vessel temp = vesselVector[0];
			vesselVector.erase(vesselVector.begin());
			futureState = vesselGenerator(vesselVector, lookAhdTime, false);
			//Generate own future coordinates
			// TODO: use myvessel instead
			vector<coord> ownFuture = myVessel.getOwnTrajectory().getCoordAhead(loopTime, lookAhdTime);
			//bool collision = chkFuture(futureState, ownFuture, t);

			bool collision = false;
			int checkcount = 0;
			collision = chkFuture(futureState, ownFuture, loopTime);
			cout << "checkcount: " << checkcount << endl;
			if (collision == true) {
			cout << "Collision Predicted!" << endl;
			//Call Optimizer
			myVessel.updateTrajectory();
			coord pt(myVessel.getLongitude(), myVessel.getLatitude(), myVessel.getCourse(), myVessel.getSpeed());
			myVessel.setOwnTrajectory(solver.getSolution(myVessel.getOwnTrajectory(), loopTime, initState, vesselVector, myVessel.getLength(), myVessel.getTrajectoryPos(), pt));
			// TODO: update myvessel instead of ownstate
			myVessel.updateTime(loopTime);
			}
			vesselVector.insert(vesselVector.begin(), temp); */
			time(&end_timer);
			cout << "simulation tick: " << loopTime << "/" << runTime
				<< ", time spent:" << difftime(end_timer, start_timer) << endl;
			// TODO: Advance own vessel + print to target history
		}

		updateDensity(loopTime, vesselVector);
		checkConflict(vesselVector, myVessel, loopTime);
	}
}

void Environment::checkConflict(vector<Vessel> vessels, OwnVessel own, int t)
{
	double lon = own.getLongitude(), lat = own.getLatitude();
	coord cd(lon, lat);
	double dist = own.getLength() * 4;
	for (int i = 1; i < vessels.size(); i++) {
		if (cpaCalc::getDistance(vessels[i], cd) < safeDis / 2.0) {
			cout << "Collision with: " << vessels[i].getName() << " " << to_string(cpaCalc::getDistance(vessels[i], cd)) << endl;
			conflictOutput << t << " " << vessels[i].getName() << endl;
		}
	}

}

void Environment::checkCollision(vector<Vessel> vessels, OwnVessel own, int t)
{
	double lon = own.getLongitude(), lat = own.getLatitude();
	coord cd(lon, lat);
	double dist = own.getLength() * 4;
	for (int i = 1; i < vessels.size(); i++) {
		if (cpaCalc::getDistance(vessels[i], cd) < safeDis / 4) {
			cout << "Collision with: " << vessels[i].getName() << " " << to_string(cpaCalc::getDistance(vessels[i], cd)) << endl;
			conflictOutput << t << " " << vessels[i].getName() << endl;
		}
	}
}

double Environment::getTriangular(double p) {
	double min = 0.0, mode = 90.0, max = 180.0;
	if (p == ((mode - min) / (max - min)))
	{
		return  mode;
	}
	else if (p < ((mode - min) / (max - min)))
	{
		return  min + sqrt(p * (max - min) * (mode - min));
	}
	else
	{
		return  max - sqrt((1 - p) * (max - min) * (max - mode));
	}
}

void Environment::printTraject(OwnTrajectory traject)
{
	vector<coord> futureCoord = traject.getCoordAhead(0, runTime);
	for (int t = 0; t < runTime; t++) {
		coord ownCoord = futureCoord[t];
		origOwnOutput << ownCoord.lon << " " << ownCoord.lat << " " << ownCoord.course << " " << t << endl;
	}
}

bool Environment::chkCollision(vector<Vessel> vesselState, coord ownCoord, int t)
{
	bool collision = false;
	for (int i = 0; i < vesselState.size(); i++) {
		if (cpaCalc::getDistance(vesselState[i], ownCoord) < safeDis) {
			collision = true;
			cout << "Collision with: " << vesselState[i].getName() << " " << to_string(cpaCalc::getDistance(vesselState[i], ownCoord)) << endl;
			printCollision("Collision with :" + vesselState[i].getName() + " " + to_string(cpaCalc::getDistance(vesselState[i], ownCoord)), t);
		}
	}
	return collision;
}

bool Environment::chkICollision(vector<Vessel> vesselState, coord ownICoord, int t) {
	bool collision = false;
	for (int i = 0; i < vesselState.size(); i++) {
		if (cpaCalc::getDistance(vesselState[i], ownICoord) < safeDis) {
			collision = true;
			//cout << "Collision with: " << vesselState[i].getName() << " ";
			printICollision("Collision with :" + vesselState[i].getName() + " " + to_string(cpaCalc::getDistance(vesselState[i], ownICoord)), t);
		}
	}
	return collision;
}

void Environment::printCollision(string vName, int t) {
	collOutput << t << " " << vName << endl;
}

void Environment::printICollision(string vName, int t) {
	icollOutput << t << " " << vName << endl;
}

void Environment::printOwn(int t) {
	coord ownCoord = ownState.getCoordAt(t);
	ownOutput << ownCoord.lon << " " << ownCoord.lat << " " << ownCoord.course << " " << t << endl;
}

void Environment::printVessel(int t)
{
	//vector<Vessel> currVessels = timeState.vesselVect[t];
	vector<Vessel> currVessels = cpaCalc::filterVessels(vesselVector, ownState.getCoordAt(t), initState.getCoordAt(t));
	for (int i = 0; i < currVessels.size(); i++) {
		vesselOutput << currVessels[i].getLongitude() << " " << currVessels[i].getLatitude() << " " << currVessels[i].getCourse() << " " << t << endl;
	}
}

bool Environment::chkFuture(vesselGenerator futureState, vector<coord> ownFuture, int t)
{
	bool collision = false;
	for (int tt = 1; tt < lookAhdTime; tt++) {//for each lookahead minute check distance of own trajectory
		for (int i = 0; i < futureState.vesselVect[0].size(); i++) {
			if (find(names.begin(), names.end(), futureState.vesselVect[tt][i].getName()) == names.end()) {
				if (cpaCalc::getDistance(futureState.vesselVect[tt][i], ownFuture[tt]) < safeDis*1.5) {
					//collision
					names.push_back(futureState.vesselVect[tt][i].getName());
					double cDist = cpaCalc::getDistance(futureState.vesselVect[tt][i], ownFuture[tt]);
					collision = true;
					//TO EDIT TO print only one at once
					cout << "Collision Predicted in " << tt << " mins! with " << futureState.vesselVect[tt][i].getName() << " @" << t << endl;
					// TODO: put back
					// manuOutput << "Collision Predicted in " << tt << " mins! with " << futureState.vesselVect[tt][i].getName() << " @" << t << endl;


					try {
						//to print out the input data for kriging
						trajInput.open(trajIPFileName);
						itrajInput.open(itrajIPFileName);
						vesselInput.open(vesselIPFileName);
						ownState.print(trajInput);
						initState.print(itrajInput);
						cpaCalc::printVessels(vesselInput, cpaCalc::filterVessels(futureState.vesselVect[0], ownFuture[0]));
						trajInput.close(); vesselInput.close(); itrajInput.close();
						//print end

					}
					catch (string error) {
						cout << error << endl;
					}

					break;
				}
			}

		}
		if (collision == true) {
			break;
		}
	}
	return collision;
}

OwnTrajectory Environment::getSolution(int t)
{
	////string evalString = "zz = krig(" + to_string(t) + ");";
	//string evalString = "zz = krigd(" + to_string(t) + "," + to_string(replication) + "," + testcase + ");";
	//engEvalString(matLab, evalString.c_str());
	//mxArray *zz = engGetVariable(matLab, "zz");

	//double *z = mxGetPr(zz);

	//cout << z[0] << " " << z[1] << " " << z[2] << " " << z[3] << endl;
	//manuOutput << z[0] << " " << z[1] << " " << z[2] << " " << z[3] << endl;
	//Manoeuvre solution(t, t + 75, z[0], z[1], z[2], z[3], ownState);
	////engEvalString(matLab, "clc;clear;");
	//return solution.fullTraj;

	OwnTrajectory temp;
	coord lonlat(103.586, 1.02735, 15);//103.586, 1.02735, 15);
	temp = temp.genDetTrajectory(1, lonlat);
	return temp;
}

void Environment::updateDensity(int t, vector<Vessel> vessels)
{
	int count = 0;

	// Square 1
	//double minLat = 1.14971, maxLat = 1.20874, minLon = 103.60588, maxLon = 103.69377;
	// Square 2
	double minLat = 1.20016, maxLat = 1.24787, minLon = 103.7741, maxLon = 103.84517;
	// Square 3
	//double minLat = 1.23277, maxLat = 1.25873, minLon = 103.70235, maxLon = 103.78303;
	// Square 4
	//double minLat = 1.20291, maxLat = 1.2338, minLon = 103.63506, maxLon = 103.7051;
	// Square 5
	//double minLat = 1.19604, maxLat = 1.21904, minLon = 103.79427, maxLon = 103.83522;

	for (int i = 0; i < vessels.size(); i++) {
		double lat = vessels[i].getLatitude();
		double lon = vessels[i].getLongitude();
		if (lat > minLat && lat < maxLat && lon > minLon && lon < maxLon) {
			count++;
		}
	}
	densityOutput /*<< t << ","*/ << count << endl;
}
