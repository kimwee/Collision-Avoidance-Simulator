#include "stdafx.h"
#include "pvector.h"


pvector::pvector(double xx, double yy)
{
	x = xx;
	y = yy;
}

pvector::pvector() {}
pvector::~pvector()
{
}
pvector pvector::operator+(const pvector&b) {
	pvector newPVector = pvector(this->x + b.x, this->y + b.y);
	return newPVector;
}
pvector pvector::operator-(const pvector&b) {
	pvector newPVector = pvector(this->x - b.x, this->y - b.y);
	return newPVector;
}
pvector pvector::operator*(double b) {
	pvector newPVector = pvector(this->x * b, this->y * b);
	return newPVector;
}
double pvector::getDistance() {
	return sqrt(x*x + y*y);
}
double pvector::getx()const {
	return this->x;
}
double pvector::gety()const {
	return this->y;
}