#pragma once
#include <iostream>
#include <string>

#define eps 0.0001
using namespace std;
using std::string;

class coord
{
private:

public:
	double lon;
	double lat;
	double course;
	double speed;
	bool chkEqual(coord target);
	coord();
	coord(double lonn, double latt);
	coord(double lonn, double latt, double speedd);
	coord(double lonn, double latt, double coursee, double speedd);
	~coord();
};
