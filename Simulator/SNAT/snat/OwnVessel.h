#pragma once
#include <string>
#include <vector>
#include <sstream>
#include "OwnTrajectory.h"

class OwnVessel
{
private:
	std::string Vessel_Name; // the indentity key for vessel, unique
	double Vessel_Length;
	double Vessel_Longitude;
	double Vessel_Latitude;
	double Vessel_Course;
	double Vessel_Speed;
	bool Vessel_Status;
	long MMSI;
	double on_pattern_lat;
	double on_pattern_lon;
	OwnTrajectory trajectory;
	int trajectoryPos = 0;
	double timeToNext = 0;
	bool onPoint = true;

public:
	OwnVessel();
	OwnVessel(std::string, double, double, double, double, double, bool);
	OwnVessel(std::ifstream &inputOwnData);
	~OwnVessel();
	std::string getName();
	bool endPoint = false;
	double getLength();
	double getLongitude();
	double getLatitude();
	double getCourse();
	double getSpeed();
	bool getStatus();
	void setLongitude(double);
	void setLatitude(double);
	void setCourse(double);
	void setSpeed(double);
	void setStatus(bool);
	void addTrajectory(std::vector<coord>);
	void updateLocation(double mt);
	void updateTime(double loopTime);
	void updateTrajectory();
	pair<double, double> LatLonToXY(double lat1, double lon1, double lat2, double lon2);
	double adjustZeroSpeed(double knot);
	double knotToKmPerMin(double knot);
	pair<double, double> XYToLatLon(double reference_lat, double reference_lon, double x, double y);
	void setOwnTrajectory(OwnTrajectory);
	OwnTrajectory getOwnTrajectory();
	void setTrajectoryPos(int);
	int getTrajectoryPos();
	void writeToOutStreams(double old_speed, double old_course, std::ofstream & outSpeedF, std::ofstream & outCourseF,
		double runtime, std::string avoidanceStr,
		std::ostream & OwnTargetHistory, std::ostream & outTargetHistory);
};

