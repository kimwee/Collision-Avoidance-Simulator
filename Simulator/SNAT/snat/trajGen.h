#include "cpaCalc.h"
#pragma once
class trajGen
{
public:
	trajGen();
	~trajGen();
	static double angleMax;
	static double angleMin;
	static double angleMaxUrs;
	static double speedMin;
	static double speedMax;
	static double getHyp(double theta2, double theta1, double minDist);
	static point getIntersect(double theta1, double absAlpha, double hypothenuse, point a);
	static coord getIntersectCoord(double theta2, double theta1, coord lonlat2, coord lonlat1);
	static double generateRandomCourse(coord lonlat2, coord lonlat1);
	static bool rangeCheckPositive(double lbound, double ubound);
	static bool rangeCheckNegative(double lbound, double ubound);
	static bool rangeValid(double lbound, double ubound);
	static bool validThetas(double theta1, double theta2);
	static void generateThetas(double &theta1, double &theta2, bool polarity);//, double alpha, double beta);
	static void generateTheta(double &theta);
	static void generateSpeeds(double &speed1, double &speed2);
	static void generateSpeed(double &speed);
};

