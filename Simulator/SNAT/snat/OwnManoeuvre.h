#pragma once
#include <iostream>
#include <string>
#include "OwnTrajectory.h"
#include "trajGen.h"
using namespace std;

class OwnManoeuvre {
public:
	double bTime;
	double eTime;
	double lookAhdTime;
	vector<double> gene;
	/*double theta1;
	double theta2;
	double speedMid;
	double speedEnd;*/
	int startIdx;
	int endIdx;
	double fitness;
	OwnTrajectory fullTraj;
	OwnTrajectory subTraj;
	OwnManoeuvre();
	OwnManoeuvre(double btime, double etime, double t1, double t2, double speedmid, double speedend, OwnTrajectory fulltraj); //Constructor
	OwnManoeuvre(double btime, double etime, double t1, double t2, double speedmid, double speedend, OwnTrajectory fulltraj, OwnTrajectory ifulltraj);
	OwnManoeuvre(double btime, double etime, double t1, double t2, double speedmid, double speedend, OwnTrajectory fulltraj, OwnTrajectory ifulltraj, int trajPos, coord pt);
	OwnManoeuvre(double btime, double etime, OwnTrajectory fulltraj, bool polarity);
	vector<coord> getCoordAhead(int timeAhead);
	void genSubTraj();
	void genSubNTraj(int trajPos, coord pt);
	void genFullTraj();
	void printManoeuvre();
	void outManoeuvre(std::ofstream & outManeuvreF);

};