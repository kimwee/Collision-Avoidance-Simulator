#pragma once
#include <random>
#define PI 3.14159

using namespace std;
class distFunc
{
public:
	distFunc();
	~distFunc();
	static poisson_distribution<int> poisson;
	static uniform_int_distribution<int> unif;
	static normal_distribution<double> norm;
	static double normGen(std::default_random_engine & gen);
	static double randGenerator(std::default_random_engine & gen); //generates a uniform random number
	static double normGenerator(std::default_random_engine & gen); //Use two random numbers to generate a random number that follows the standard normal distribution
	static int poissonGenerator(std::default_random_engine & gen);
	static int uniGenerator(std::default_random_engine & gen);
	static double lognormGenerator(double mean, double std, std::default_random_engine & gen);
	static bool randSpeed(std::default_random_engine & gen);
	static bool randCourse(std::default_random_engine & gen);
	static double randSpeedChange(std::default_random_engine & generator);
	static double randCourseChange(std::default_random_engine & generator);
	static double max(double a, double b);
	static double min(double a, double b);
};

