#pragma once
#include "engine.h"
#include "OwnTrajectory.h"
#include "OwnManoeuvre.h"
#include "vesselGenerator.h"


#define BUFSIZE 1000

class Optimiser
{
private:
	Engine* matlab;

public:
	Optimiser();
	~Optimiser();
	OwnTrajectory getSolution(OwnTrajectory, int, OwnTrajectory, vector<Vessel>, double, int, coord);
	double y1Mean(vector<pair<double, double>>);
	double y1MeanNew(vector<double>);
	double y2Mean(vector<pair<double, double>>);
	double y2MeanNew(vector<double>);
	double v1Var(vector<pair<double, double>>,double);
	double v1VarNew(vector<double>);
	double v2Var(vector<pair<double, double>>,double);
	double v2VarNew(vector<double>);
	vector<vector<double>> krigSol(vector<vector<double>>, vector<double>, vector<double>, vector<double>, vector<double>);
	double* generateArray(vector<vector<double>> x_cand);
	bool checkCollision(vesselGenerator futureState, vector<coord> ownFuture, double vesselLength);
	vector<int> OCBA(vector<double>, vector<double>, int);
};

