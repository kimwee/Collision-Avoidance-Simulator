#pragma once

#include "course.h"
#include "coord.h"
#include <math.h>
#include "Vessel.h"
#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <random>
#define PI 3.14159
#define RAD 6371000 //in metres
#define SMALL_NUM   0.00000001
#define dot(u,v)   ((u).x * (v).x + (u).y * (v).y)
#define norm(v)    sqrt(dot(v,v))  // norm = length of  vector
#define d(u,v)     norm(u-v)        // distance = norm of difference
#define abs(x)     ((x) >= 0 ? (x) : -(x))   //  absolute value
#define solnPerimeter 10000 //in metres. 15 knots = approx 28kph = 7km/15mins

class cpaCalc
{

public:
	enum CollType
	{
		headOnL,//355-360
		headOnR,//0-5
		crossLF,//270-355
		crossLR,//247.5-270
		crossRF,//5-90
		crossRR,//90-112.5
		ovrtkL,//180-247.5
		ovrtkR//112.5-180
	};
	static double cpa_time(course Tr1, course Tr2);
	static double cpa_distance(course Tr1, course Tr2);
	static void toLonLat(coord &lonlat, pvector vect);
	static double toRadius(double degree);
	static double toDegree(double radian);
	static void toXY(point &p2, point &p1, coord lonlat2, coord lonlat1);
	static double getBearing(coord lonlat2, coord lonlat1);
	static double getDistance(Vessel lonlat2, coord lonlat1);
	static double getDistance(coord lonlat2, coord lonlat1);
	static CollType getColType(double ownCourse, double tgtCourse);
	static double cosinDiff(pvector a, pvector b);
	static double randBetween(double a, double b);
	static int randBetweenInt(int a, int b);
	static bool coinToss();
	static vector<Vessel> loadVessels(ifstream &readFile);
	static void printVessels(ofstream &outFile, vector<Vessel> &vesselVect);
	static void deleteFile(string fileName);
	static vector<Vessel> filterVessels(vector<Vessel> vesselVect, coord ownCoord);
	static vector<Vessel> filterVessels(vector<Vessel> vesselVect, coord ownCoord, coord ownICoord);
};

