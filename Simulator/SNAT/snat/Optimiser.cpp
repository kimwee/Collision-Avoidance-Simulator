#include "stdafx.h"
#include "Optimiser.h"

#define EPSI 0.000000

Optimiser::Optimiser()
{
	matlab = engOpen(NULL);
	//if (matlab == NULL) {
	//	printf("Can't start MATLAB engine!!!\n");
	//	exit(-1);
	//}


	char buffer[BUFSIZE + 1];
	buffer[BUFSIZE] = '\0';
	engOutputBuffer(matlab, buffer, BUFSIZE);
	engEvalString(matlab, "clc;clear;");
	engEvalString(matlab, "cd \'C:\\Users\\ASUS\\Dropbox\\Simulation Files\\JiaHao\\MATLAB-Krig'");

	//mxArray *a = mxCreateDoubleScalar(2); // assume a=2
	//mxArray *b = mxCreateDoubleScalar(3); // assume b=3
	//engPutVariable(matlab, "a", a); // put into matlab
	//engPutVariable(matlab, "b", b); // put into matlab

	//engEvalString(matlab, "a");
	//engEvalString(matlab, "b");
	//engEvalString(matlab, "y = a + b");

	//mxArray *y = engGetVariable(matlab, "y");
	//double y_res = mxGetScalar(y);
	//printf("y=%f\n", y_res);

	//engEvalString(matlab, "a = [1 2 3; 4 5 6]");
	//mxArray *a = engGetVariable(matlab, "a");
	//double *arr = mxGetPr(a);
	//for (int i = 0; i < 6; i++) {
	//	cout << arr[i] << endl;
	//}

	//int d = 4;
	//int n0 = 20;
	//string evalString = "l = lhsdesign(" + to_string(n0) + "," + to_string(d) + ");";
	//engEvalString(matlab, evalString.c_str());
	//mxArray *m = engGetVariable(matlab, "l");
	//double *ptr = mxGetPr(m);
	//vector<vector<double>> x_cand;
	//for (int i = 0; i < n0; i++) {
	//	vector<double> temp;
	//	for (int j = 0; j < d; j++) {
	//		temp.push_back(ptr[i + j*n0]);
	//	}
	//	x_cand.push_back(temp);
	//}
	//int b = 1;
}


Optimiser::~Optimiser()
{
}

OwnTrajectory Optimiser::getSolution(OwnTrajectory x, int bTime, OwnTrajectory initState, vector<Vessel> currState, double len, int trajPos, coord pt)
{ 
	int d = 4;
	int n0 = 20;
	int rep = 10;
	int lookAhead = 20;
	double safety = 0.04;
	string evalString = "l = lhsdesign(" + to_string(n0) + "," + to_string(d) + ");";
	engEvalString(matlab, evalString.c_str());
	mxArray *m = engGetVariable(matlab, "l");
	double *ptr = mxGetPr(m);
	vector<vector<double>> x_cand;
	for (int i = 0; i < n0; i++) {
		vector<double> temp;
		for (int j = 0; j < d; j++) {
			temp.push_back(ptr[i + j*n0]);
		}
		x_cand.push_back(temp);
	}
	std::default_random_engine generator;
	vector<double> y1, y2, v1, v2;


	vector<vector<double>> y_temp;
	// DO NEGATIVE FIRST
	double input[4];
	double angleMax = 0, angleMin = -75, speedMin = 1, speedMax = 20;
	for (int i = 0; i < n0/2; i++) {
		input[0] = x_cand[i][0] * (angleMax - angleMin) + angleMin;
		input[1] = x_cand[i][1] * (angleMax - angleMin) + angleMin;
		input[2] = x_cand[i][2] * (speedMax - speedMin) + speedMin;
		input[3] = x_cand[i][3] * (speedMax - speedMin) + speedMin;
		OwnManoeuvre candidate(bTime, bTime + 15 + 60, input[0], input[1], input[2], input[3], x, initState, trajPos, pt);
		double minDist = 6 * len;
		vector<coord> manuCoord = candidate.fullTraj.getCoordAhead(bTime, lookAhead);
		vector<pair<double,double>> output;
		vector<double> y_col;
		for (int j = 0; j < rep; j++) {//loop through replications
									   //generate coordinates of all vessels for each replication
			vesselGenerator vesselMatrix(currState, lookAhead, true);


			bool collision = false;
			for (int t = 0; t < vesselMatrix.timeFrame + 1; t++) {//loop through time

				for (int v = 0; v < vesselMatrix.vesselVect[0].size(); v++) {//loop through all vessels
					Vessel target = vesselMatrix.vesselVect[t][v];
					coord currCoord = manuCoord[t];
					double distance = cpaCalc::getDistance(target, currCoord);
					if (distance < minDist) {//collision criteria
											 //cout << t << " " << target.getName() << endl;
						collision = true;
						break;
					}
				}
				if (collision == true) { break; }
			}
			pair<double,double> temp;
			if (collision == true) {
				temp.first = 1 + distFunc::normGen(generator);
				y_col.push_back(true);
			}
			else { 
				temp.first = 0 + distFunc::normGen(generator); 
				y_col.push_back(false);
			}
			temp.second = candidate.fitness + distFunc::normGen(generator);
			output.push_back(temp);
		}
		y_temp.push_back(y_col);
		y1.push_back(y1MeanNew(y_col));
		v1.push_back(v1VarNew(y_col));
//		double m1 = y1Mean(output);
//		y1.push_back(m1);
		double m2 = y2Mean(output);
		y2.push_back(m2);
//		v1.push_back(v1Var(output,m1));
		v2.push_back(v2Var(output,m2));
	}

	// Positive section
	angleMin = 0; angleMax = 75;
	for (int i = n0/2; i < n0; i++) {
		input[0] = x_cand[i][0] * (angleMax - angleMin) + angleMin;
		input[1] = x_cand[i][1] * (angleMax - angleMin) + angleMin;
		input[2] = x_cand[i][2] * (speedMax - speedMin) + speedMin;
		input[3] = x_cand[i][3] * (speedMax - speedMin) + speedMin;
		OwnManoeuvre candidate(bTime, bTime + 15 + 60, input[0], input[1], input[2], input[3], x, initState, trajPos, pt);
		double minDist = 6 * len;
		vector<coord> manuCoord = candidate.fullTraj.getCoordAhead(bTime, lookAhead);
		vector<pair<double, double>> output;
		vector<double> y_col;
		for (int j = 0; j < rep; j++) {//loop through replications
									   //generate coordinates of all vessels for each replication
			vesselGenerator vesselMatrix(currState, lookAhead, true);


			bool collision = false;
			for (int t = 0; t < vesselMatrix.timeFrame + 1; t++) {//loop through time

				for (int v = 0; v < vesselMatrix.vesselVect[0].size(); v++) {//loop through all vessels
					Vessel target = vesselMatrix.vesselVect[t][v];
					coord currCoord = manuCoord[t];
					double distance = cpaCalc::getDistance(target, currCoord);
					if (distance < minDist) {//collision criteria
											 //cout << t << " " << target.getName() << endl;
						collision = true;
						break;
					}
				}
				if (collision == true) { break; }
			}
			pair<double, double> temp;
			if (collision == true) {
				temp.first = 1 + distFunc::normGen(generator);
				y_col.push_back(true);
			}
			else {
				temp.first = 0 + distFunc::normGen(generator);
				y_col.push_back(false);
			}
			temp.second = candidate.fitness + distFunc::normGen(generator);
			output.push_back(temp);
		}
		y_temp.push_back(y_col);
		y1.push_back(y1MeanNew(y_col));
		v1.push_back(v1VarNew(y_col));
		//		double m1 = y1Mean(output);
		//		y1.push_back(m1);
		double m2 = y2Mean(output);
		y2.push_back(m2);
		//		v1.push_back(v1Var(output,m1));
		v2.push_back(v2Var(output, m2));
	}

	// get y2 & v2
	// init y_temp & y_train
	//for (int c = 0; c < n0; c++) {
	//	for (int r = 0; r < rep; r++) {
	//		// call check solution
	//	}
	//	// update y1,v1 
	//}

	int k = n0;
	double sim_budget = 500; // ss - rep * n0;
	int B = 100; // budget per iteration
	int min_bdg = 10;
	while (sim_budget > 0) {
		vector<vector<double>> newSol = krigSol(x_cand, y1, v1, y2, v2);
		x_cand.push_back(newSol[0]);
		x_cand.push_back(newSol[1]);
		// pushback both new solution from krigSol(x_cand[1:k-1],y1,v1,y2,v2);
		vector<double> y1temp1, y1temp2, y2temp1, y2temp2;
		vesselGenerator vesselMatrix(currState, lookAhead, true);
		// negative
		angleMax = 0; angleMin = -75;
		input[0] = x_cand[k][0] * (angleMax - angleMin) + angleMin;
		input[1] = x_cand[k][1] * (angleMax - angleMin) + angleMin;
		input[2] = x_cand[k][2] * (speedMax - speedMin) + speedMin;
		input[3] = x_cand[k][3] * (speedMax - speedMin) + speedMin;
		OwnManoeuvre candidate1(bTime, bTime + 15, input[0], input[1], input[2], input[3], x, initState, trajPos, pt);
		vector<coord> sol1 = candidate1.fullTraj.getCoordAhead(bTime, lookAhead);
		// positive
		angleMin = 0; angleMax = 75;
		input[0] = x_cand[k + 1][0] * (angleMax - angleMin) + angleMin;
		input[1] = x_cand[k + 1][1] * (angleMax - angleMin) + angleMin;
		input[2] = x_cand[k + 1][2] * (speedMax - speedMin) + speedMin;
		input[3] = x_cand[k + 1][3] * (speedMax - speedMin) + speedMin;
		OwnManoeuvre candidate2(bTime, bTime + 15, input[0], input[1], input[2], input[3], x, initState, trajPos, pt);
		vector<coord> sol2 = candidate2.fullTraj.getCoordAhead(bTime, lookAhead);
		for (int r = 0; r < min_bdg; r++) {
			// y_temp check collision
			y1temp1.push_back(checkCollision(vesselMatrix, sol1, x.vesselLength));
			y1temp2.push_back(checkCollision(vesselMatrix, sol2, x.vesselLength));
			y2temp1.push_back(candidate1.fitness + distFunc::normGen(generator));
			y2temp2.push_back(candidate2.fitness + distFunc::normGen(generator));
		}
		y_temp.push_back(y1temp1);
		y_temp.push_back(y1temp2);
		// pushback new probability of collision mean/var, dist from orig mean/var
		y1.push_back(y1MeanNew(y1temp1));
		v1.push_back(v1VarNew(y1temp1));
		y1.push_back(y1MeanNew(y1temp2));
		v1.push_back(v1VarNew(y1temp2));
		y2.push_back(y1MeanNew(y2temp1));
		v2.push_back(v1VarNew(y2temp1));
		y2.push_back(y1MeanNew(y2temp2));
		v2.push_back(v1VarNew(y2temp2));

		// call OCBA and store into replicates
		vector<int> replicates = OCBA(y1, v1, B - min_bdg);

		//for (int c = 0; c < x_cand.size(); c++) { // size or size + 1
		//	if (c < n0 / 2 || (c >= n0 && c % 2 == 1)) { // positive
		//		angleMin = 0; angleMax = 75;
		//	}
		//	else { // negative
		//		angleMax = 0; angleMin = -75;
		//	}
		//	for (int r = 0; r < replicates[c]; r++) {	 // 1 replaced by size of replicates at c
		//		input[0] = x_cand[c][0] * (angleMax - angleMin) + angleMin;
		//		input[1] = x_cand[c][1] * (angleMax - angleMin) + angleMin;
		//		input[2] = x_cand[c][2] * (speedMax - speedMin) + speedMin;
		//		input[3] = x_cand[c][3] * (speedMax - speedMin) + speedMin;
		//		OwnManoeuvre candidate(bTime, bTime + 15 + 60, input[0], input[1], input[2], input[3], x, initState);
		//		vector<coord> sol = candidate.fullTraj.getCoordAhead(bTime, lookAhead);
		//		y_temp[c].push_back(checkCollision(vesselMatrix, sol, x.vesselLength));
		//	}
		//	// update all the values of v1,y1,v2,y2
		//	y1[c] = y1MeanNew(y_temp[c]);
		//	v1[c] = v1VarNew(y_temp[c]);
		//	y2[c] = y1MeanNew(y_temp[c]);
		//	v2[c] = v1VarNew(y_temp[c]);
		//}
		k += 2;
		sim_budget -= 2*B;
	}

	int idx = 0;
	double minDist = y2[0];
	bool isFound = false;

	for (int i = 1; i < y1.size(); i++) {
		if (y1[i] < safety) {
			if (y2[i] < minDist) {
				idx = i;
				minDist = y2[i];
				isFound = true;
			}
		}
	}

	if (!isFound) {
		for (int i = 1; i < y1.size(); i++) {
			if (y2[i] < minDist) {
				idx = i;
				minDist = y2[i];
			}
		}
	}

	if (idx < n0 / 2 || (idx >= n0 && idx % 2 == 1)) { // positive
		angleMin = 0; angleMax = 75;
	}
	else { // negative
		angleMax = 0; angleMin = -75;
	}
	input[0] = x_cand[idx][0] * (angleMax - angleMin) + angleMin;
	input[1] = x_cand[idx][1] * (angleMax - angleMin) + angleMin;
	input[2] = x_cand[idx][2] * (speedMax - speedMin) + speedMin;
	input[3] = x_cand[idx][3] * (speedMax - speedMin) + speedMin;
	//OwnManoeuvre candidate(bTime, bTime + 15 + 60, input[0], input[1], input[2], input[3], x, initState);
	OwnManoeuvre candidate(bTime, bTime + 15, input[0], input[1], input[2], input[3], x, initState, trajPos, pt);
//	candidate.genFullTraj();
	return candidate.fullTraj;
}

double Optimiser::y1Mean(vector<pair<double, double>> output)
{
	double sum = 0;
	for (int i = 0; i < output.size(); i++) {
		sum += output[i].first;
	}
	return sum / output.size();
}

double Optimiser::y1MeanNew(vector<double> output)
{
	double sum = 0;
	for (int i = 0; i < output.size(); i++) {
		sum += output[i];
	}
	return sum / output.size();
}

double Optimiser::y2Mean(vector<pair<double, double>> output)
{
	double sum = 0;
	for (int i = 0; i < output.size(); i++) {
		sum += output[i].second;
	}
	return sum / output.size();
}

double Optimiser::y2MeanNew(vector<double>)
{
	return 0.0;
}

double Optimiser::v1Var(vector<pair<double, double>> output, double mean)
{
	double sum = 0;
	for (int i = 0; i < output.size(); i++) {
		sum += (output[i].first - mean) * (output[i].first - mean);
	}
	return sum / (output.size() - 1);
}

double Optimiser::v1VarNew(vector<double> output)
{
	double sum = 0;
	for (int i = 0; i < output.size(); i++) {
		sum += output[i];
	}
	double mean = sum / output.size();
	sum = 0;
	for (int i = 0; i < output.size(); i++) {
		sum += (output[i] - mean) * (output[i] - mean);
	}
	return sum / (output.size() - 1);

}

double Optimiser::v2Var(vector<pair<double, double>> output, double mean)
{
	double sum = 0;
	for (int i = 0; i < output.size(); i++) {
		sum += (output[i].second - mean) * (output[i].second - mean);
	}
	return sum / (output.size() - 1);
}

vector<vector<double>> Optimiser::krigSol(vector<vector<double>> x_cand, vector<double> y1, vector<double> v1, vector<double> y2, vector<double> v2)
{
	// TODO
	int size = x_cand.size();
	mwSize dims[] = { size,4 };
	mxArray *m = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
	double *ptr = (double*)mxGetData(m);
	size = size * 4;
	double* data = new double[size];
	int index = 0;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < x_cand.size(); j++) {	
			data[index] = x_cand[j][i];
			index++;
		}
	}

	memcpy(ptr, &data[0], size*sizeof(double));
	engPutVariable(matlab, "x_cand", m);
	delete[] data;

	size = y1.size();
	mwSize dim[] = { size };
	m = mxCreateNumericArray(1, dim, mxDOUBLE_CLASS, mxREAL);
	ptr = (double*)mxGetData(m);
	data = new double[size];
	for (int i = 0; i < size; i++) {
		data[i] = y1[i];
	}
	memcpy(ptr, &data[0], size*sizeof(double));
	engPutVariable(matlab, "y1", m);
	delete[] data;

	size = v1.size();
	dim[0] = size;
	m = mxCreateNumericArray(1, dim, mxDOUBLE_CLASS, mxREAL);
	ptr = (double*)mxGetData(m);
	data = new double[size];
	for (int i = 0; i < size; i++) {
		data[i] = v1[i];
	}
	memcpy(ptr, &data[0], size*sizeof(double));
	engPutVariable(matlab, "v1", m);
	delete[] data;

	size = y2.size();
	dim[0] = size;
	m = mxCreateNumericArray(1, dim, mxDOUBLE_CLASS, mxREAL);
	ptr = (double*)mxGetData(m);
	data = new double[size];
	for (int i = 0; i < size; i++) {
		data[i] = y2[i];
	}
	memcpy(ptr, &data[0], size*sizeof(double));
	engPutVariable(matlab, "y2", m);
	delete[] data;

	size = v2.size();
	dim[0] = size;
	m = mxCreateNumericArray(1, dim, mxDOUBLE_CLASS, mxREAL);
	ptr = (double*)mxGetData(m);
	data = new double[size];
	for (int i = 0; i < size; i++) {
		data[i] = v2[i];
	}
	memcpy(ptr, &data[0], size*sizeof(double));
	engPutVariable(matlab, "v2", m);
	delete[] data;

	engEvalString(matlab, "[krign,krigp] = krigSol(x_cand, y1, v1, y2, v2)");

	m = engGetVariable(matlab, "krign");
	ptr = mxGetPr(m);
	vector<double> krign;
	for (int j = 0; j < 4; j++) {
		krign.push_back(ptr[j]);
	}
	m = engGetVariable(matlab, "krigp");
	ptr = mxGetPr(m);
	vector<double> krigp;
	for (int j = 0; j < 4; j++) {
		krigp.push_back(ptr[j]);
	}
	vector<vector<double>> ans;
	ans.push_back(krign);
	ans.push_back(krigp);
	return ans;
}

double * Optimiser::generateArray(vector<vector<double>> x_cand)
{
	return nullptr;
}

bool Optimiser::checkCollision(vesselGenerator futureState, vector<coord> ownFuture, double vesselLength)
{
	bool collision = false;
	for (int tt = 1; tt < 10; tt++) {//for each lookahead minute check distance of own trajectory
		for (int i = 0; i < futureState.vesselVect[0].size(); i++) {
			if (cpaCalc::getDistance(futureState.vesselVect[tt][i], ownFuture[tt]) < vesselLength * 4 *1.5) {
				//collision
				double cDist = cpaCalc::getDistance(futureState.vesselVect[tt][i], ownFuture[tt]);
				collision = true;
				break;
			}
		}
		if (collision == true) {
			break;
		}
	}
	return collision;
}

vector<int> Optimiser::OCBA(vector<double> y1, vector<double> v1, int bal)
{
	string evalString = "rep = OCBA(0,y1,v1," + to_string(bal) + ");";
	engEvalString(matlab, evalString.c_str());

	mxArray *m = engGetVariable(matlab, "rep");
	double *ptr = mxGetPr(m);
	vector<int> rep;
	for (int j = 0; j < y1.size(); j++) {
		rep.push_back(ptr[j]);
	}
	return rep;
}
