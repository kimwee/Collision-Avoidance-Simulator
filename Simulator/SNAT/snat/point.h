#pragma once
#include <iostream>
#include <string>
#include "pvector.h"

using namespace std;
using std::string;

class point
{
private:

public:
	double x;
	double y;
	double distTo(const point&b);
	pvector operator-(const point&b);
	point operator+(const pvector&b);
	point();
	point(double xx, double yy);
	~point();
};

