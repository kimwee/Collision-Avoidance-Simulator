#include "stdafx.h"
#include "OwnManoeuvre.h"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

//ofstream manoeuvreFile ("Manoeuvre.txt", ios::app);

OwnManoeuvre::OwnManoeuvre() {}

//initial fulltraj is provided
OwnManoeuvre::OwnManoeuvre(double btime, double etime, double t1, double t2, double speedmid, double speedend, OwnTrajectory fulltraj)
{
	bTime = btime;
	eTime = etime;
	gene.resize(4);
	gene[0] = t1;
	gene[1] = t2;
	gene[2] = speedmid;
	gene[3] = speedend;
	/*theta1 = t1;
	theta2 = t2;
	speedMid = speedmid;
	speedEnd = speedend;*/
	fullTraj = fulltraj;
	genSubTraj();
	genFullTraj();
	int first, last;
	fitness = fulltraj.getSubTrajectory(bTime, eTime, first, last).getGeoFitness(subTraj);
}
OwnManoeuvre::OwnManoeuvre(double btime, double etime, double t1, double t2, double speedmid, double speedend, OwnTrajectory fulltraj, OwnTrajectory ifulltraj)
{
	bTime = btime;
	eTime = etime;
	gene.resize(4);
	gene[0] = t1;
	gene[1] = t2;
	gene[2] = speedmid;
	gene[3] = speedend;
	/*theta1 = t1;
	theta2 = t2;
	speedMid = speedmid;
	speedEnd = speedend;*/
	fullTraj = fulltraj;
	genSubTraj();
	genFullTraj();
	int first, last;
	fitness = ifulltraj.getGeoFitness(fullTraj);
}

OwnManoeuvre::OwnManoeuvre(double btime, double etime, double t1, double t2, double speedmid, double speedend, OwnTrajectory fulltraj, OwnTrajectory ifulltraj, int trajPos, coord pt)
{
	bTime = btime;
	eTime = etime;
	gene.resize(4);
	gene[0] = t1;
	gene[1] = t2;
	gene[2] = speedmid;
	gene[3] = speedend;
	/*theta1 = t1;
	theta2 = t2;
	speedMid = speedmid;
	speedEnd = speedend;*/
	fullTraj = fulltraj;
	genSubNTraj(trajPos, pt);
	genFullTraj();
	int first, last;
	fitness = ifulltraj.getGeoFitness(fullTraj);
}

//inital fulltraj is provided, generate random manoeuvre
OwnManoeuvre::OwnManoeuvre(double btime, double etime, OwnTrajectory fulltraj, bool polarity)
{
	bTime = btime;
	eTime = etime;
	fullTraj = fulltraj;
	gene.resize(4);
	/*coord init, end;
	init = fullTraj.getCoordAt(bTime);
	end = fullTraj.getCoordAt(eTime);*/
	//double absAlpha, alpha, beta;//absAlpha is bearing from init to end, alpha is change in course from init to end
	trajGen::generateThetas(gene[0], gene[1], polarity);
	trajGen::generateSpeeds(gene[2], gene[3]);
	genSubTraj();
	genFullTraj();
	int first, last;
	fitness = fulltraj.getSubTrajectory(bTime, eTime, first, last).getGeoFitness(subTraj);
}

//returns coordinates for next timeAhead minutes
vector<coord> OwnManoeuvre::getCoordAhead(int timeAhead) {
	return fullTraj.getCoordAhead(bTime, timeAhead);
}

//subtrajectory will only consist of three points, time of first coord will be bTime
//generate a subtrajectory using bTime, eTime, and the midpoint according to thetas and speeds
void OwnManoeuvre::genSubTraj() {
	subTraj.points.push_back(fullTraj.getCoordAt(bTime));//extract first coord
	subTraj.timeVect.push_back(bTime);//extract first time
	coord midCoord, endCoord;
	endCoord = (fullTraj.getCoordAt(eTime));//extract last coord
											//double dist = cpaCalc::getDistance(subTraj.trajectVect[0], endCoord);//used for debugging to check how much distance allowed for manipulation
	endCoord.speed = gene[3];//speedEnd assigned
	subTraj.points.push_back(endCoord);//push back last coord
	midCoord = trajGen::getIntersectCoord(gene[1], gene[0], subTraj.points.back(), subTraj.points.front());//generate midpoint from thetas and speeds
	midCoord.speed = gene[2];//speedMid assigned
	subTraj.insertCoord(midCoord, 1);//insert midpt into subtrajectory
	fullTraj.chkNodes(bTime, eTime, startIdx, endIdx);//only to update startIdx and lastIdx
}

//subtrajectory will only consist of three points, time of first coord will be bTime
//generate a subtrajectory using bTime, eTime, and the midpoint according to thetas and speeds
void OwnManoeuvre::genSubNTraj(int trajPos, coord pt) {
	subTraj.points.push_back(pt);
	bTime = fullTraj.timeVect[trajPos];
	subTraj.timeVect.push_back(bTime);//extract first time
	eTime = bTime + 15;


//	subTraj.points.push_back(fullTraj.getCoordAt(bTime));//extract first coord
//	subTraj.timeVect.push_back(bTime);//extract first time
	coord midCoord, endCoord;
	endCoord = (fullTraj.getCoordAt(eTime));//extract last coord
											//double dist = cpaCalc::getDistance(subTraj.trajectVect[0], endCoord);//used for debugging to check how much distance allowed for manipulation
	endCoord.speed = gene[3];//speedEnd assigned
	subTraj.points.push_back(endCoord);//push back last coord
	midCoord = trajGen::getIntersectCoord(gene[1], gene[0], subTraj.points.back(), subTraj.points.front());//generate midpoint from thetas and speeds
	midCoord.speed = gene[2];//speedMid assigned
	subTraj.insertCoord(midCoord, 1);//insert midpt into subtrajectory
	fullTraj.chkNodes(bTime, eTime, startIdx, endIdx);//only to update startIdx and lastIdx
}

//merge subtrajectory into fill trajectory
void OwnManoeuvre::genFullTraj() {
	if (subTraj.points.size() == 0) { return; }
	fullTraj.combineTrajectory(subTraj, startIdx, endIdx);
}

//print details to cout
void OwnManoeuvre::printManoeuvre()
{
	cout << bTime << " " << eTime << " " << gene[0] << " " << gene[1] << " " << gene[2] << " " << gene[3] << endl;
}

//print maneuvre details to an ofstream
void OwnManoeuvre::outManoeuvre(std::ofstream & manoeuvreFile)
{
	manoeuvreFile << bTime << " " << eTime << " " << gene[0] << " " << gene[1] << " " << gene[2] << " " << gene[3] << endl;
}
