#include "stdafx.h"
#include "cpaCalc.h"


double cpaCalc::cosinDiff(pvector a, pvector b) {
	double num = dot(a, b);
	double den = a.getDistance() * b.getDistance();
	return num / den;
}
// cpa_time(): compute the time of CPA for two tracks
//    Input:  two tracks Tr1 and Tr2
//    Return: the time at which the two tracks are closest
double cpaCalc::cpa_time(course Tr1, course Tr2)
{
	pvector   dv = Tr1.vect - Tr2.vect;

	double    dv2 = dot(dv, dv);
	if (dv2 < SMALL_NUM)      // the  tracks are almost parallel
		return 0.0;             // any time is ok.  Use time 0.

	pvector   w0 = Tr1.pt - Tr2.pt;
	double    cpatime = -dot(w0, dv) / dv2;

	return cpatime;             // time of CPA
}
//===================================================================


// cpa_distance(): compute the distance at CPA for two tracks
//    Input:  two tracks Tr1 and Tr2
//    Return: the distance for which the two tracks are closest
double cpaCalc::cpa_distance(course Tr1, course Tr2)
{
	double    ctime = cpa_time(Tr1, Tr2);
	point    P1 = Tr1.pt + (Tr1.vect*ctime);
	point    P2 = Tr2.pt + (Tr2.vect*ctime);

	return d(P1, P2);            // distance at CPA
}
double cpaCalc::toRadius(double degree) //Unit conversion from degree to radius
{
	return degree * PI / 180.0;
}

double cpaCalc::toDegree(double radian) //Unit conversion from degree to radius
{
	return radian * 180 / PI;
}

void cpaCalc::toLonLat(coord &lonlat, pvector vect) {
	//Coordinate offsets in radians
	double dLat = vect.y / RAD;
	double dLon = vect.x / (RAD*cos(toRadius(lonlat.lat)));

	//OffsetPosition, decimal degrees
	lonlat.lat = lonlat.lat + dLat * 180 / PI;
	lonlat.lon = lonlat.lon + dLon * 180 / PI;
}
//returns p2 as xy from coord1, p1 as origin
void cpaCalc::toXY(point &p2, point &p1, coord lonlat2, coord lonlat1) {
	p1 = point(0, 0);
	double x, y;
	y = RAD * toRadius(lonlat2.lat - lonlat1.lat);
	x = RAD * toRadius(lonlat2.lon - lonlat1.lon) * cos(toRadius(lonlat1.lat));
	p2 = point(x, y);
}

double cpaCalc::getBearing(coord lonlat2, coord lonlat1) //To calculate course of segment between two coordinates in radians
{
	//returns initial bearing from 1 to 2 wrt North in radians
	double diffLo = toRadius(lonlat2.lon) - toRadius(lonlat1.lon);
	double numerator = sin(diffLo)*cos(toRadius(lonlat2.lat));
	double denominator = cos(toRadius(lonlat1.lat))*sin(toRadius(lonlat2.lat)) - sin(toRadius(lonlat1.lat))*cos(toRadius(lonlat2.lat))*cos(diffLo);
	double twoPI = 2 * PI;
	double bearing = fmod(atan2(numerator, denominator), twoPI);
	if (bearing < 0) { bearing += twoPI; }
	return bearing;
}
double cpaCalc::getDistance(coord lonlat2, coord lonlat1) //To calculate distance between two coordinates
{
	//positive coordinates indicate N/W for longitude/latitude while negative coordinates indicate S/E
	double diffLo = toRadius(lonlat1.lon) - toRadius(lonlat2.lon);
	double diffLa = toRadius(lonlat1.lat) - toRadius(lonlat2.lat);
	double constantA = sin(diffLa / 2)*sin(diffLa / 2) + cos(toRadius(lonlat1.lat))*cos(toRadius(lonlat2.lat))*sin(diffLo / 2)*sin(diffLo / 2);
	double constantB = 2 * atan2(sqrt(constantA), sqrt(1 - constantA));
	double distance = RAD * constantB;
	return distance;
}
double cpaCalc::getDistance(Vessel lonlat2, coord lonlat1) //To calculate distance between two coordinates
{
	//positive coordinates indicate N/W for longitude/latitude while negative coordinates indicate S/E
	double diffLo = toRadius(lonlat1.lon) - toRadius(lonlat2.getLongitude());
	double diffLa = toRadius(lonlat1.lat) - toRadius(lonlat2.getLatitude());
	double constantA = sin(diffLa / 2)*sin(diffLa / 2) + cos(toRadius(lonlat1.lat))*cos(toRadius(lonlat2.getLatitude()))*sin(diffLo / 2)*sin(diffLo / 2);
	double constantB = 2 * atan2(sqrt(constantA), sqrt(1 - constantA));
	double distance = RAD * constantB;
	return distance;
}
cpaCalc::CollType cpaCalc::getColType(double ownCourse, double tgtCourse) {
	double dCourse = fmod(540 - (ownCourse - tgtCourse), 360);
	cout << dCourse << endl;
	if (dCourse <= 5) {
		return headOnR;//0-5
	}
	else if (dCourse <= 90) {
		return crossRF;//5-90
	}
	else if (dCourse <= 112.5) {
		return crossRR;//90-112.5
	}
	else if (dCourse <= 180) {
		return ovrtkR;//112.5-180
	}
	else if (dCourse <= 247.5) {
		return ovrtkL;//180-247.5
	}
	else if (dCourse <= 270) {
		return crossLR;//247.5-270
	}
	else if (dCourse <= 355) {
		return crossLF;//270-355
	}
	else {
		return 	headOnL;//355-360
	}
}


double cpaCalc::randBetween(double a, double b) {
	double f = (double)rand() / RAND_MAX;
	return a + f * (b - a);
}
int cpaCalc::randBetweenInt(int a, int b) {
	uniform_int_distribution<int> uni(a, b);
	default_random_engine gen;
	return uni(gen);
}

bool cpaCalc::coinToss() {
	return (randBetween(0, 1) > 0.5);
}

vector<Vessel> cpaCalc::loadVessels(ifstream &readFile) {
	string read;
	vector<Vessel> vesselVect;
	while (getline(readFile, read)) //Read file
	{
		string aName;
		double aLength, aLongitude, aLatitude, aCourse, aSpeed;
		istringstream is(read);
		is >> aName;
		is >> aLength;
		is >> aLongitude;
		is >> aLatitude;
		is >> aCourse;
		is >> aSpeed;
		Vessel aVessel(aName, aLength, aLongitude, aLatitude, aCourse, aSpeed, false);
		vesselVect.push_back(aVessel);
	}
	return vesselVect;
}
void cpaCalc::printVessels(ofstream &outFile, vector<Vessel> &vesselVect) {
	for (vector<Vessel>::iterator it = vesselVect.begin(); it != vesselVect.end(); ++it) {
		outFile << it->getName() << " " << it->getLength() << " " << it->getLongitude() << " " << it->getLatitude() << " " << it->getCourse() << " " << it->getSpeed() << endl;
	}
}
void cpaCalc::deleteFile(string fileName) {
	if (remove(fileName.c_str())) {
		return;
	}
}
vector<Vessel> cpaCalc::filterVessels(vector<Vessel> vesselVect, coord ownCoord) {
	for (int i = vesselVect.size() - 1; i >= 0; i--)
		if (getDistance(vesselVect[i], ownCoord) > solnPerimeter) {
			vesselVect.erase(vesselVect.begin() + i);
		}
	return vesselVect;
}
vector<Vessel> cpaCalc::filterVessels(vector<Vessel> vesselVect, coord ownCoord, coord ownICoord) {
	for (int i = vesselVect.size() - 1; i >= 0; i--)
		if (getDistance(vesselVect[i], ownCoord) > solnPerimeter && getDistance(vesselVect[i], ownICoord) > solnPerimeter) {
			vesselVect.erase(vesselVect.begin() + i);
		}
	return vesselVect;
}