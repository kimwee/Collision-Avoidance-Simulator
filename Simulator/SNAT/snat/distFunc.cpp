#include "stdafx.h"
#include "distFunc.h"

poisson_distribution<int> distFunc::poisson(4);
uniform_int_distribution<int> distFunc::unif(5, 10);
normal_distribution<double> distFunc::norm(0, 0.01);
distFunc::distFunc()
{
}


distFunc::~distFunc()
{
}
int distFunc::uniGenerator(std::default_random_engine & gen) {
	return unif(gen);
}
double distFunc::randGenerator(std::default_random_engine & gen)
{

	//std::default_random_engine generator(12345); //the seed helps to repeat the experiment
	std::uniform_real_distribution<double> distribution(0, 1);
	//double aRand = distribution(generator);
	double aRand = distribution(gen);
	return aRand;
}
double distFunc::normGen(std::default_random_engine & gen) {
	//std::default_random_engine gen;
	return norm(gen);
}
double distFunc::normGenerator(std::default_random_engine & gen) //Use two random numbers to generate a random number that follows the standard normal distribution
{
	double normRand = sqrt(-2 * 2.303*log(randGenerator(gen)))*cos(2 * PI*randGenerator(gen));
	return normRand;
}
double distFunc::lognormGenerator(double mean, double std, std::default_random_engine & gen)
{
	double normRand = normGenerator(gen);
	double lognormRand = exp(mean + std*normRand);
	return lognormRand;
}
bool distFunc::randSpeed(std::default_random_engine & gen)
{
	if (randGenerator(gen) <= 0.12) //There will be a random change in speed
		return true;
	else //There will not be a random change in speed
		return false;

}
int distFunc::poissonGenerator(std::default_random_engine & gen) {
	return poisson(gen);
}
bool distFunc::randCourse(std::default_random_engine & gen)
{
	if (randGenerator(gen) <= 0.37) //There will be a random change in course
		return true;
	else //There will not be a random change in course
		return false;
}

double distFunc::randSpeedChange(std::default_random_engine & generator)
{
	double schange; //Percentage of change in speed
	if (randSpeed(generator))
	{

		if (randGenerator(generator) <= 0.27) //The change will be an increase in speed
		{
			schange = lognormGenerator(-0.99487, 0.77507, generator);
			return schange;
		}
		else //The change will be a decrease in speed
		{
			schange = -0.57881 + 0.19179*normGenerator(generator);
			return schange;
		}
	}
	else
		return 0.0;
}

double distFunc::randCourseChange(std::default_random_engine & generator)
{
	double cchange; //Change in course in degree
	if (randCourse(generator))
	{
		if (randGenerator(generator) <= 0.49) //The change will be a right turn
		{
			cchange = max(0, min(60, 103 + 46.42626*normGenerator(generator)));
			//cchange = 103.05195 + 46.42626*normGenerator(generator);
			return cchange;
		}
		else //The change will be a left turn
		{
			cchange = max(-60, min(0, -98.53705 + 45.7462*normGenerator(generator)));
			//cchange = -98.53705 + 45.7462*normGenerator(generator);
			return cchange;
		}
	}
	else
		return 0.0;
}

double distFunc::max(double a, double b)
{
	return (a > b) ? a : b;
}

double distFunc::min(double a, double b)
{
	return (a < b) ? a : b;
}
