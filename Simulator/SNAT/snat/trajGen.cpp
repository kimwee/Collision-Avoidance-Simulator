#include "stdafx.h"
#include "trajGen.h"

double trajGen::angleMax = 60;
double trajGen::angleMin = 0;
double trajGen::angleMaxUrs = 90;
double trajGen::speedMax = 15;
double trajGen::speedMin = 1;



trajGen::trajGen()
{
}


trajGen::~trajGen()
{
}

//checks if range is more than zero
bool trajGen::rangeCheckPositive(double lbound, double ubound) {
	if (lbound > ubound) {
		return true;
	}
	if (lbound > 0) {
		return true;
	}
	else {
		return false;
	}
}
bool trajGen::rangeCheckNegative(double lbound, double ubound) {
	if (lbound > ubound) {
		return true;
	}
	if ((ubound <0)) {
		return true;
	}
	else {
		return false;
	}
}
bool trajGen::rangeValid(double lbound, double ubound) {
	if (lbound < ubound) {
		return true;
	}
	else {
		return false;
	}
}
bool trajGen::validThetas(double theta1, double theta2) {
	double sum = theta1 + theta2;
	if (theta1 > 0 && theta2 > 0) {
		if (sum > angleMin && sum < angleMax) {
			return true;
		}
		else return false;
	}
	else if (theta1 < 0 && theta2 < 0) {
		if (sum < -angleMin && sum > -angleMax) {
			return true;
		}
		else return false;
	}
	else return false;
}
//void trajGen::generateThetas(double &theta1, double &theta2, double alpha, double beta){
void trajGen::generateThetas(double &theta1, double &theta2, bool polarity) {
	///*
	//angleMin-alpha < theta1 < angleMax-alpha (a1, a2)
	//or -angleMax - alpha < theta1 < -angleMin - alpha (b1, b2)
	//angleMin-beta < theta2 < angleMax-beta (c1, c2)
	//or -angleMax - beta < theta2 < -angleMin - beta (d1, d2) 

	//if theta1 > 0, theta2 > 0
	//angleMin < theta1 + theta2 < angleMax or -angleMax < theta1 + theta2 < -angleMin
	//*/
	//double a1, a2, b1, b2, c1, c2, d1, d2;
	//a1 = angleMin - alpha;
	//a2 = angleMax - alpha;
	//b1 = -angleMax - alpha;
	//b2 = -angleMin - alpha;
	//c1 = angleMin - beta;
	//c2 = angleMax - beta;
	//d1 = -angleMax - beta;
	//d2 = -angleMin - beta;
	//double theta1UBound, theta1LBound, theta2UBound, theta2LBound;
	//double aLBound, aUBound, bLBound, bUBound;
	//aLBound = max(a1, -angleMax);
	//bLBound = max(b1, -angleMax);
	//aUBound = min(a2, angleMax);
	//bUBound = min(b2, angleMax);
	//if (coinToss()) {
	//	theta1UBound = aUBound;
	//	theta1LBound = bLBound;
	//}
	//else {
	//	theta1UBound = aUBound;
	//	theta1LBound = bLBound; 
	//}
	//theta1 = randBetween(theta1LBound, theta1UBound);
	//double cLBound, cUBound, dLBound, dUBound;
	//cLBound = max(double(0), max(c1, angleMin - theta1));
	//cUBound = min(c2, angleMax - theta1);
	//dLBound = max(double(0), max(d1, angleMin - theta1));
	//dUBound = min(d2, angleMax - theta1);
	//if (theta1 > 0){
	//	/*if (rangeCheckNegative(d1, d2)) {
	//		theta2LBound = cLBound;
	//		theta2UBound = cUBound;
	//	}*/
	//	if (coinToss() && rangeValid(dLBound, dUBound)) {
	//		theta2LBound = dLBound;
	//		theta2UBound = dUBound;
	//	}
	//	else if(rangeValid(cLBound, cUBound) ){
	//		theta2LBound = cLBound;
	//		theta2UBound = cUBound;
	//	}
	//	else {
	//		cout << "Out of Range!!" << endl;
	//		return;
	//	}
	//}	else {
	//	/*if (rangeCheckPositive(c1, c2)){
	//		theta2LBound = max(d1, -angleMax - theta1);
	//		theta2UBound = min(double(0), min(d2, -angleMin - theta1));
	//	}*/
	//	cLBound = max(c1, -angleMax - theta1);
	//	cUBound = min(c2, min(double(0), -angleMin - theta1));
	//	dLBound = max(d1, -angleMax - theta1);
	//	dUBound = min(double(0), min(d2, -angleMin - theta1));
	//	if (coinToss() && rangeValid(cLBound,cUBound)) {
	//		theta2LBound = cLBound;
	//		theta2UBound = cUBound;
	//	}
	//	else if (rangeValid(dLBound, dUBound)) {
	//		theta2LBound = dLBound;
	//		theta2UBound = dUBound;
	//	}
	//	else {
	//		cout << "Out of Range!" << endl;
	//		return;
	//	}
	//}
	//cout << theta1LBound << " " << theta1UBound << endl;
	//cout << theta2LBound << " " << theta2UBound << endl;
	//theta2 = randBetween(theta2LBound, theta2UBound);
	//if (!validThetas(theta1, theta2)){
	generateTheta(theta1);
	generateTheta(theta2);
	if (polarity) {//generate positive thetas
		return;
	}
	else {
		theta1 = -1 * theta1;
		theta2 = -1 * theta2;
	}

	//}
}
void trajGen::generateTheta(double &theta) {
	theta = cpaCalc::randBetween(0, angleMax);
}
void trajGen::generateSpeeds(double &speed1, double &speed2) {
	speed1 = cpaCalc::randBetween(speedMin, speedMax);
	speed2 = cpaCalc::randBetween(speedMin, speedMax);
	return;
}
void trajGen::generateSpeed(double &speed) {
	speed = cpaCalc::randBetween(speedMin, speedMax);
	return;
}
double trajGen::getHyp(double theta2, double theta1, double minDist) {
	double tan1Reciprocal, tan2Reciprocal;
	tan1Reciprocal = 1 / tan(-theta1);
	tan2Reciprocal = 1 / tan(-theta2);
	double triangleHt = minDist / (tan1Reciprocal + tan2Reciprocal);
	return triangleHt / sin(-theta1);
}

//getIntersect: return the intersection as a point
// Input: in radians- theta1, alpha. Hypothenuse from the end point, initial point
point trajGen::getIntersect(double theta1, double absAlpha, double hypothenuse, point a) {
	double absTheta1 = absAlpha + theta1;
	point intersect(hypothenuse*sin(absTheta1) + a.x, hypothenuse*cos(absTheta1) + a.y);
	return intersect;
}
coord trajGen::getIntersectCoord(double theta2, double theta1, coord lonlat2, coord lonlat1) {
	double hypo = getHyp(cpaCalc::toRadius(theta2), cpaCalc::toRadius(theta1), cpaCalc::getDistance(lonlat2, lonlat1));
	double absAlpha = cpaCalc::getBearing(lonlat2, lonlat1);
	point initial(0, 0);
	//toXY(initial, point(0, 0), lonlat2, lonlat1);
	point intersect = getIntersect(cpaCalc::toRadius(theta1), absAlpha, hypo, initial);
	coord intersectCoord = lonlat1;
	cpaCalc::toLonLat(intersectCoord, intersect - point(0, 0));
	intersectCoord.course = cpaCalc::toDegree(cpaCalc::getBearing(lonlat2, intersectCoord));
	return intersectCoord;
}
double trajGen::generateRandomCourse(coord lonlat2, coord lonlat1) {
	double absAlpha = cpaCalc::toDegree(cpaCalc::getBearing(lonlat2, lonlat1));
	double rngAdd = cpaCalc::randBetween(120, 240);
	double course1 = fmod(rngAdd + absAlpha, 360);
	return course1 + 180;
}