#pragma once
#include <vector>
#include "Vessel.h"
#include "Constants.h"
#include "PathPlanner.h"
#include "PathMover.h"
#include "Trajectory.h"
#include "TrajectoryLoader.h"
#include "UnitTester.h"
#include "PathMoverTrajectoryBased.h"
#include "Environment.h"
#include "OwnTrajectory.h"
#include "OwnVessel.h"
#include "coord.h"
#include "vesselGenerator.h"
#include "Optimiser.h"

#include <cstdio> 
#include <random>
#include <Windows.h>
#include <algorithm>
#include <sstream>
#include <ppl.h>

using namespace std;

class Environment
{
private:
	vector<Vessel> surrVessels;
	vector<string> names;
	vector<Trajectory> allPatterns;
	vector<TrajectoryPoint> portEndPoints;
	time_t start_timer, end_timer;
	string errorMsg;
	int runTime;
	int currTime;
	vector<Vessel> vesselVector;
	double safeDis;
	double MT;
	double conditionT;
	double mt; 
	int numIndex;
	int lookAhdTime;
	double safeFactor;
	double checkSafeDis;
	vector<double> numVector;
	string strAdd;
	OwnVessel myVessel;
	OwnTrajectory ownState;
	OwnTrajectory initState;
	PathMoverTrajectoryBased pattern_based_mover;
	ofstream origOwnOutput;
	ofstream collOutput;
	ofstream icollOutput;
	ofstream vesselOutput;
	ofstream ownOutput;
	ofstream fitnessOutput;
	ofstream trajInput;
	ofstream itrajInput;
	ofstream vesselInput;
	ofstream manuOutput;
	ofstream densityOutput;
	ofstream conflictOutput;
	ofstream collisionOutput;
	string trajIPFileName;
	string itrajIPFileName;
	string vesselIPFileName;
	Optimiser solver;

public:
	Environment();
	~Environment();
	std::default_random_engine generator;
	bool isValidation = false;
	double getTriangular(double p);
	void initialiseEnvironment(int argc, char *argv[]);
	void initialiseVEnvironment(int argc, char *argv[]);
	void assignPatterns();
	void close();
	void runSimulation();
	void runVSimulation();
	void checkConflict(vector<Vessel> vessels, OwnVessel own, int t);
	void checkCollision(vector<Vessel> vessels, OwnVessel own, int t);
	void printTraject(OwnTrajectory traject);
	bool chkCollision(vector<Vessel> vesselState, coord ownCoord, int t);
	void printCollision(string vName, int t);
	bool chkICollision(vector<Vessel> vesselState, coord ownICoord, int t);
	void printICollision(string vName, int t);
	void printOwn(int t);
	void printVessel(int t);
	bool chkFuture(vesselGenerator futureState, vector<coord> ownFuture, int t);
	OwnTrajectory getSolution(int t);
	void updateDensity(int t, vector<Vessel> vessels);
};

