#pragma once
#include <ppl.h>
#include "Vessel.h"
#include "distFunc.h"
#include "cpaCalc.h"

#include "PathPlanner.h"
#include "PathMover.h"
#include "Trajectory.h"
#include "TrajectoryLoader.h"
#include "Constants.h"
#include "PathMoverTrajectoryBased.h"
#include <string>
#include <sstream>
#include <fstream>

using namespace Concurrency;
class vesselGenerator
{
	//vesselGenerator generates coordinates for next t mins for one iteration
public:
	std::default_random_engine generator;
	vector<vector<Vessel>> vesselVect;//outer refers to time, inner dimension refers to vessel
	int timeFrame;
	vesselGenerator(vector<Vessel> vesselvect, int timeframe, bool type);//true types are random, false types are deterministic
																		 //vesselGenerator(ifstream &readFile);
	~vesselGenerator();
	vesselGenerator();
	void advanceRand();
	void advanceDeter();
	void loadTrajectory();
	double max(double a, double b);
	double min(double a, double b);
};

