#include "stdafx.h"
#include "OwnTrajectory.h"


OwnTrajectory::OwnTrajectory()
{
}


OwnTrajectory::~OwnTrajectory()
{
}

void OwnTrajectory::addPoints(vector<coord> myPoints)
{
	points = myPoints;
}

OwnTrajectory OwnTrajectory::genDetTrajectory(int direction, coord initLonLat)
{
	OwnTrajectory traject;
	coord coordA = initLonLat;
	int xDir, yDir;
	switch (direction) {
	case 1:
	default:
		xDir = 1;
		yDir = 1;
		break;
	case 2:
		xDir = 1;
		yDir = -1;
		break;
	case 3:
		xDir = -1;
		yDir = -1;
		break;
	case 4:
		xDir = -1;
		yDir = 1;
		break;
	}
	for (int i = 0; i < 40; i++) {
		point a(-500, 500);
		cpaCalc::toLonLat(coordA, a - point(0, 0));
		double speed = 3;
		coordA.speed = speed;
		traject.pushCoord(coordA);
	}
	return traject;

}

void OwnTrajectory::pushCoord(coord lonlat)
{
	double time;
	if (points.size() != 0) {
		double distance = cpaCalc::getDistance((points.back()), lonlat);
		time = distance / (lonlat.speed * knotToMetrePMin);
	}
	else time = 0;
	points.push_back(lonlat);
	if (timeVect.size() != 0) {
		timeVect.push_back((timeVect.back()) + time);
	}
	else timeVect.push_back(time);
	updateCourse();
}

void OwnTrajectory::updateCourse()
{
	if (points.size() >= 2) {
		for (vector<coord>::iterator it = points.begin(); it != points.end() - 1; ++it) {
			(*it).course = cpaCalc::toDegree(cpaCalc::getBearing(*(it + 1), *it));
		}
	}
	points.back().course = 0;
}

int OwnTrajectory::getIndexBefore(double time)
{
	int indexBefore = getIndexAfter(time) - 1;
	if (indexBefore < 0) {
		return 9999;
	}
	else return indexBefore;
}

int OwnTrajectory::getIndexAfter(double time)
{
	//return index of the next coordinate after specified time
	int i = 0;
	for (vector<double>::iterator it = timeVect.begin(); it != timeVect.end(); ++it, i++) {
		if (*it > time) {
			return i;
		}//if loop through, all time smaller
	}
	return i;
}

//returns coordinates for next timeAhead + 1 minutes e.g. 0,15 returns 16 coordinates
vector<coord> OwnTrajectory::getCoordAhead(int bTime, int timeAhead)
{
	//vector<coord> coordVect;
	//for (int t = bTime; t < bTime + timeAhead; t++){
	//	coordVect.push_back(getCoordAt(t));
	//}
	//return coordVect;
	if (bTime + timeAhead + 1 > timeVect.back()) {
		vector<coord> emptyVect;
		return emptyVect;
	}
	vector<coord> coordVect;
	int currPre, currPost;//currPre is index of coordinate before stated time, currPost is index of after
	point dest;
	double xdist, ydist, timeDiff, preTime;

	//following segment initialises the variables
	currPre = getIndexBefore(bTime);
	currPost = currPre + 1;
	cpaCalc::toXY(dest, point(0, 0), points[currPre], points[currPost]);
	preTime = timeVect[currPre];
	xdist = dest.x; ydist = dest.y;
	timeDiff = timeVect[currPre] - timeVect[currPost];

	for (int t = bTime; t < bTime + timeAhead + 1; t++) {
		if (t > timeVect[currPre]) {//if time is larger than current postIndex time
			currPre = getIndexBefore(t);
			currPost = currPre + 1;
			cpaCalc::toXY(dest, point(0, 0), points[currPre], points[currPost]);//update dest to match new postindex
			xdist = dest.x; ydist = dest.y;
			preTime = timeVect[currPre];
			timeDiff = timeVect[currPre] - timeVect[currPost];
		}
		double ratio = (t - preTime) / timeDiff;
		point midPt = point(ratio*xdist, ratio*ydist);
		coord coordA = points[currPre];
		cpaCalc::toLonLat(coordA, midPt - point(0, 0));
		coordA.speed = points[currPost].speed;//assign previous speed
		coordVect.push_back(coordA);
	}
	return coordVect;
}

//return coordinate at given time
coord OwnTrajectory::getCoordAt(double time)
{
	if (time < timeVect.back()) {
		int before, after;
		before = getIndexBefore(time);
		after = before + 1;
		point dest;
		cpaCalc::toXY(dest, point(0, 0), points[after], points[before]);
		double xdist, ydist, timeDiff;
		xdist = dest.x; ydist = dest.y;
		timeDiff = timeVect[after] - timeVect[before];
		double ratio = (time - timeVect[before]) / timeDiff;
		point midPt = point(ratio*xdist, ratio*ydist);
		coord coordA = points[before];
		cpaCalc::toLonLat(coordA, midPt - point(0, 0));
		coordA.speed = points[after].speed;//assign previous speed
		return coordA;
	} return coord(0, 0);//invalid entry
}

void OwnTrajectory::print(ofstream & outFile)
{
	outFile << vesselLength << endl;
	int i = 0;
	for (vector<coord>::iterator it = points.begin(); it != points.end(); ++it, i++)
	{
		outFile << it->lon << " " << it->lat << " " << it->course << " " << it->speed << " " << timeVect[i] << endl;
	}
}

//returns a new subtrajectory consisting of points from bTime to eTime, and returns index of the immediate coordinate before and after stated period
//if period out of range, return entire trajectory instead
OwnTrajectory OwnTrajectory::getSubTrajectory(double bTime, double eTime, int & first, int & last)
{
	vector<coord> tVect;
	vector<double> timeV;
	if (timeVect.back() > eTime) {
		coord start = getCoordAt(bTime);
		tVect.push_back(start);
		timeV.push_back(bTime);
		coord end = getCoordAt(eTime);
		if (chkNodes(bTime, eTime, first, last)) {//if nodes present, getSub nodes and pump into vector
			OwnTrajectory sub = getSubTrajectory(first, last);
			tVect.insert(tVect.end(), sub.points.begin(), sub.points.end());
			timeV.insert(timeV.end(), sub.timeVect.begin(), sub.timeVect.end());
		}//else skip getting nodes
		tVect.push_back(end);
		timeV.push_back(eTime);
		return OwnTrajectory(tVect, timeV);
	}
	else return *this;
}

///returns a new subtrajectory consisting of points from first to last inclusive
OwnTrajectory OwnTrajectory::getSubTrajectory(int first, int last)
{
	vector<coord> tVect;
	vector<double> timeV;
	last = last + 1;
	if (points.size() > last) {
		for (vector<coord>::iterator it = points.begin() + first; it != points.begin() + last; ++it) {
			tVect.push_back(*it);
		}
		for (vector<double>::iterator it = timeVect.begin() + first; it != timeVect.begin() + last; ++it) {
			timeV.push_back(*it);
		}
		OwnTrajectory traj(tVect, timeV);
		return traj;
	}
	else return *this;
}

//create trajectory given vector of coordinates and time, assumes time is calculated properly
OwnTrajectory::OwnTrajectory(vector<coord> tVect, vector<double> timeV)
{
	for (vector<coord>::iterator it = tVect.begin(); it != tVect.end(); ++it) {
		points.push_back(*it);
	}
	for (vector<double>::iterator it = timeV.begin(); it != timeV.end(); ++it) {
		timeVect.push_back(*it);
	}
}

//checks for any nodes within specified timeframe and returns the indexes
//-1 if no nodes, and returns false; indexes of nodes and true returned otherwise
bool OwnTrajectory::chkNodes(double bTime, double eTime, int & startIdx, int & endIdx)
{
	int bIndex, eIndex;
	bIndex = getIndexAfter(bTime);
	eIndex = getIndexBefore(eTime);

	if (bIndex <= eIndex) {
		startIdx = bIndex;
		endIdx = eIndex;
		return true;
	}
	else {
		startIdx = -1;
		endIdx = -1;
		return false;
	}
}

//returns the difference in geodistance between own and testTraject
double OwnTrajectory::getGeoFitness(OwnTrajectory testTraject)
{
	double ownGeo, othGeo;
	ownGeo = calculateGeoDist(*this);
	othGeo = calculateGeoDist(testTraject);
	double ownEndTime, othEndTime;
	ownEndTime = timeVect.back();
	othEndTime = testTraject.timeVect.back();
	double timeDiff = (othEndTime - ownEndTime);
	if (timeDiff < 0) {
		timeDiff = timeDiff * -2;
	}
	return abs(othGeo - ownGeo) *(1 + timeDiff / ownEndTime);//should be positive
}

//calculates geometric distance
//NOTE:Geometric distance with own trajectory not equal zero
double OwnTrajectory::calculateGeoDist(OwnTrajectory testTraject)
{
	/*
	Geographic DIstance comprises three components
	Component A = distance between centres of mass
	Component B = dist between centre of mass *( (difference between distance of trajectories)/(max(distance of trajectories) )
	Component C = - average(|displacement|) * cosinSimilarity
	*/
	double a, b, c;

	coord ctrOwn, ctrOther;
	ctrOwn = getCtrCoord(); ctrOther = testTraject.getCtrCoord();
	vector<point> ownXY = getXYVector(), othXY = testTraject.getXYVector();
	point own = getCtrPt(), other = testTraject.getCtrPt();
	a = cpaCalc::getDistance(ctrOwn, ctrOther);

	double distOwn, distOth;
	distOwn = getTrajDist(); distOth = testTraject.getTrajDist();
	b = a * ((abs(distOwn - distOth)) / (max(distOwn, distOth)));

	pvector dispOwn, dispOth;
	dispOwn = getTrajDisp(); dispOth = testTraject.getTrajDisp();
	c = -((dispOwn.getDistance() + dispOth.getDistance()) / 2) * (cpaCalc::cosinDiff(dispOwn, dispOth));
	return a + b + c;
}

//returns centre of mass in coord lonlat form
coord OwnTrajectory::getCtrCoord()
{
	if (points.size() < 2) { return coord(0, 0); }
	point ctr = getCtrPt();
	coord coordCtr = points.front();
	cpaCalc::toLonLat(coordCtr, ctr - point(0, 0));
	return coordCtr;
}

//returns centre of mass in xy form
point OwnTrajectory::getCtrPt()
{
	if (points.size() < 2) { return point(0, 0); }
	//double sumSqDiffX=0, sumDiffX=0;
	//double sumSqDiffY=0, sumDiffY=0;
	double cumX, cumY;
	double prevX, currX;
	double prevY, currY;
	vector<point> pointVector = getXYVector();
	prevX = pointVector.front().x;
	prevY = pointVector.front().y;
	for (vector<point>::iterator it = pointVector.begin() + 1; it != pointVector.end(); ++it) {
		currX = (*it).x; currY = (*it).y;
		/*sumSqDiffX += (currX*currX) - (prevX*prevX);
		sumSqDiffY += (currY*currY) - (prevY*prevY);
		sumDiffX += (currX - prevX);
		sumDiffY += (currY - prevY);*/
		cumX = currX + prevX; cumY = currY + prevY;
		prevX = currX; prevY = currY;
	}
	double ptX, ptY;
	//ptX = sumSqDiffX / (2 * sumDiffX);
	//ptY = sumSqDiffY / (2 * sumDiffY);
	ptX = cumX / points.size();
	ptY = cumY / points.size();
	point ctr(ptX, ptY);
	return ctr;
}

//returns trajectory coordiantes in XY form, used in geodist
vector<point> OwnTrajectory::getXYVector()
{
	vector<point> pointVector;
	if (points.size() > 0) {
		point origin(0, 0), nextPt;
		coord originLonlat = points.front();

		pointVector.push_back(origin);
		for (vector<coord>::iterator it = points.begin() + 1; it != points.end(); ++it) {
			cpaCalc::toXY(nextPt, origin, *it, originLonlat);
			pointVector.push_back(nextPt);
		}
		return pointVector;
	}
	else return pointVector;
}

//return distance of trajectory
double OwnTrajectory::getTrajDist()
{
	if (points.size() < 2) { return 0; }
	double cumDist = 0;
	coord prev, curr;
	prev = *points.begin();
	for (vector<coord>::iterator it = points.begin() + 1; it != points.end(); ++it) {
		curr = *it;
		cumDist += cpaCalc::getDistance(curr, prev);
		prev = curr;
	}
	return cumDist;
}

//returns xy representation of trajectory displacement
pvector OwnTrajectory::getTrajDisp()
{
	point origin(0, 0), endPt;
	coord originLonlat = points.front();
	cpaCalc::toXY(endPt, origin, points.back(), originLonlat);
	return (endPt - origin);
}

//inserts coordinate into trajectory right before given index
void OwnTrajectory::insertCoord(coord lonlat, int index)
{
	if (points.size() > index) {
		points.insert(points.begin() + index, lonlat);
		updateTime(timeVect.front());
		updateCourse();
	}
	else {
		points.insert(points.begin(), lonlat);
		updateTime(timeVect.front());
		updateCourse();
	}
}

//updates time vector given set of coordinates
void OwnTrajectory::updateTime()
{
	timeVect.clear();
	if (points.size()>0) {
		coord lonlat1, lonlat2;
		lonlat1 = *(points.begin());
		double cumTime = 0;
		timeVect.push_back(0);//first entry time is zero
		for (vector<coord>::iterator it = points.begin() + 1; it != points.end(); ++it) {
			lonlat2 = *it;
			double distance = cpaCalc::getDistance(lonlat2, lonlat1);
			double time = distance / (lonlat2.speed * knotToMetrePMin);
			cumTime = cumTime + time;
			timeVect.push_back(cumTime);
			lonlat1 = lonlat2;
		}
	}
}

//updates time vector given set of coordinates and start time
void OwnTrajectory::updateTime(double init)
{
	timeVect.clear();
	if (points.size()>0) {
		coord lonlat1, lonlat2;
		lonlat1 = *(points.begin());
		double cumTime = init;
		timeVect.push_back(init);//first entry time is zero
		for (vector<coord>::iterator it = points.begin() + 1; it != points.end(); ++it) {
			lonlat2 = *it;
			double distance = cpaCalc::getDistance(lonlat2, lonlat1);
			double time = distance / (lonlat2.speed * knotToMetrePMin);//speed in knots = 1852m/60mins, 
			cumTime = cumTime + time;
			timeVect.push_back(cumTime);
			lonlat1 = lonlat2;
		}
	}
}

//lIndex should index of last replaced node. element at lIndex will be removed
//merge subtrajectory into main trajectory, given indices right before and after desired positions
void OwnTrajectory::combineTrajectory(OwnTrajectory subTrajectory, int startIdx, int endIdx)
{
	double bTime, eTime;
	bTime = *(subTrajectory.timeVect.begin());
	eTime = subTrajectory.timeVect.back();
	coord start, end;
	start = getCoordAt(bTime);
	end = getCoordAt(eTime);
	//if (start.chkEqual(*(subTrajectory.points.begin())) == false) {
	//	return;//not equal coordinates, cannot merge
	//}
	if (startIdx != -1 && endIdx != -1) {
		points.erase(points.begin() + startIdx, points.begin() + endIdx + 1);
	}
	int insertIdx = getIndexAfter(bTime);
	points.insert(points.begin() + insertIdx, subTrajectory.points.begin(), subTrajectory.points.end());
	updateTime(*(timeVect.begin()));
	updateCourse();
}
