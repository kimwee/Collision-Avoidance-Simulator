#pragma once
#include <vector>
#include "coord.h"
#include "point.h"
#include "cpaCalc.h"
#define knotToMetrePMin 30.87 // one knot = 1.852kph = 1852metreperhour = 1852/60 metre per min
using namespace std;

class OwnTrajectory
{
public:
	vector<coord> points;
	vector<double> timeVect;
	double vesselLength;
	OwnTrajectory();
	~OwnTrajectory();
	void addPoints(vector<coord> myPoints);
	OwnTrajectory genDetTrajectory(int direction, coord initLonLat);
	void pushCoord(coord point);
	void updateCourse();
	int getIndexBefore(double time);
	int getIndexAfter(double time);//return index of the next coordinate after specified time
	vector<coord> getCoordAhead(int bTime, int timeAhead);
	coord getCoordAt(double time);
	void print(ofstream &outFile);
	OwnTrajectory getSubTrajectory(double bTime, double eTime, int &first, int &last);
	OwnTrajectory getSubTrajectory(int first, int last);//returns a new subtrajectory consisting of points from first to last inclusive
	OwnTrajectory(vector<coord> tVect, vector<double> timeV);
	bool chkNodes(double bTime, double eTime, int &startIdx, int &endIdx);
	double getGeoFitness(OwnTrajectory testTraject);
	double calculateGeoDist(OwnTrajectory testTraject);
	coord getCtrCoord();
	point getCtrPt();
	vector<point> getXYVector();
	double getTrajDist();
	pvector getTrajDisp();
	void insertCoord(coord lonlat, int index);
	void updateTime();//updates time vector
	void updateTime(double init);//updates time vector
	void combineTrajectory(OwnTrajectory subTrajectory, int startIdx, int endIdx);
};

