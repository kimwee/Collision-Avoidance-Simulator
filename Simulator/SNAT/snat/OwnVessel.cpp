#include "stdafx.h"
#include "OwnVessel.h"
#include <fstream>

using std::string;

OwnVessel::OwnVessel()
{
}

OwnVessel::OwnVessel(std::string aName, double aLength, double aLongitude, double aLatitude, double aCourse, double aSpeed, bool aStatus)
{
	Vessel_Name = aName;
	Vessel_Length = aLength;
	Vessel_Longitude = aLongitude;
	Vessel_Latitude = aLatitude;
	Vessel_Course = aCourse;
	Vessel_Speed = aSpeed;
	Vessel_Status = aStatus;
}

OwnVessel::OwnVessel(std::ifstream &inputOwnData)
{
	//string read;
	//getline(inputOwnData, read);
	//string aName;
	//double aLength, aLongitude, aLatitude, aCourse, aSpeed;
	//istringstream is(read);
	//is >> Vessel_Name;
	//is >> Vessel_Length;
	//is >> Vessel_Longitude;
	//is >> Vessel_Latitude;
	//is >> Vessel_Course;
	//is >> Vessel_Speed;

	//vector<TrajectoryPoint> myTrajectory;
	//while (!inputOwnData.eof()) //Read file
	//{
	//	getline(inputOwnData, read);
	//	if (read != "") {
	//		double navigation_status, rate_of_turn, speed_over_ground,
	//			latitude, longitude, course_over_ground, true_heading;
	//		istringstream is_path(read);
	//		is_path >> navigation_status;
	//		is_path >> rate_of_turn;
	//		is_path >> speed_over_ground;
	//		is_path >> latitude;
	//		is_path >> longitude;
	//		is_path >> course_over_ground;
	//		is_path >> true_heading;
	//		TrajectoryPoint this_trajecotory_point(latitude, longitude, rate_of_turn,
	//			course_over_ground / 10.0, true_heading, speed_over_ground);
	//		myTrajectory.push_back(this_trajecotory_point);
	//	}
	//}
	//trajectory.addPoints(myTrajectory);
}


OwnVessel::~OwnVessel()
{
}

std::string OwnVessel::getName()
{
	return Vessel_Name;
}

double OwnVessel::getLength()
{
	return Vessel_Length;
}

double OwnVessel::getLongitude()
{
	return Vessel_Longitude;
}

double OwnVessel::getLatitude()
{
	return Vessel_Latitude;
}

double OwnVessel::getCourse()
{
	return Vessel_Course;
}

double OwnVessel::getSpeed()
{
	return Vessel_Speed;
}

bool OwnVessel::getStatus()
{
	return Vessel_Status;
}

void OwnVessel::setLongitude(double longitude)
{
	Vessel_Longitude = longitude;
}

void OwnVessel::setLatitude(double lat)
{
	Vessel_Latitude = lat;
}

void OwnVessel::setCourse(double course)
{
	Vessel_Course = course;
}

void OwnVessel::setSpeed(double speed)
{
	Vessel_Speed = speed;
}

void OwnVessel::setStatus(bool status)
{
	Vessel_Status = status;
}

void OwnVessel::addTrajectory(std::vector<coord> myTrajectory)
{
	trajectory.addPoints(myTrajectory);
}

void OwnVessel::updateLocation(double mt)
{
	// TODO: UPDATE LOCATION OF OWN VESSEL
	int end = trajectory.points.size();
	if (trajectoryPos >= end-1) { // end of point
		//remain unchanged
		endPoint = true;
	}
	else {
		if (timeToNext < mt) { // enough time to move forward
			mt -= timeToNext;
			if (timeToNext != 0) {
				timeToNext = 0;
				trajectoryPos++;
				timeToNext = trajectory.timeVect[trajectoryPos] - trajectory.timeVect[trajectoryPos - 1];
			}
			else {
				trajectoryPos++;
				timeToNext = trajectory.timeVect[trajectoryPos] - trajectory.timeVect[trajectoryPos-1];
			}
		}
		else if (timeToNext == mt) { // exactly enough time
			trajectoryPos++;
			timeToNext = trajectory.timeVect[trajectoryPos] - trajectory.timeVect[trajectoryPos - 1];
			Vessel_Latitude = trajectory.points[trajectoryPos].lat;
			Vessel_Longitude = trajectory.points[trajectoryPos].lon;
			Vessel_Speed = trajectory.points[trajectoryPos].speed;
			Vessel_Course = trajectory.points[trajectoryPos].course;
			return;
		}
		else { // time to next too big
			timeToNext -= mt;
			// calc between and set (speed doesn't change)
			coord curr = coord(Vessel_Longitude, Vessel_Latitude);
			coord next = trajectory.points[trajectoryPos + 1];
			pair<double, double> direction = LatLonToXY(curr.lat, curr.lon, next.lat, next.lon);
			direction.first = direction.first / sqrt(direction.first * direction.first + direction.second * direction.second);
			direction.second = direction.second / sqrt(direction.first * direction.first + direction.second * direction.second);

			double dx = knotToKmPerMin(adjustZeroSpeed(Vessel_Speed)) * direction.first *mt;
			double dy = knotToKmPerMin(adjustZeroSpeed(Vessel_Speed)) * direction.second *mt;
			pair<double, double> new_lat_lon = XYToLatLon(Vessel_Latitude, Vessel_Longitude, dx, dy);
			Vessel_Latitude = new_lat_lon.first;
			Vessel_Longitude = new_lat_lon.second;
			return;
		}
		vector<double> time = trajectory.timeVect;
		while (trajectoryPos < end && time[trajectoryPos + 1] - time[trajectoryPos] < mt) {
			mt -= (time[trajectoryPos + 1] - time[trajectoryPos]);
			trajectoryPos++;
			Vessel_Latitude = trajectory.points[trajectoryPos].lat;
			Vessel_Longitude = trajectory.points[trajectoryPos].lon;
			Vessel_Speed = trajectory.points[trajectoryPos].speed;
			Vessel_Course = trajectory.points[trajectoryPos].course;
			timeToNext = trajectory.timeVect[trajectoryPos] - trajectory.timeVect[trajectoryPos - 1];
		}
		if (trajectoryPos >= end) {
			Vessel_Latitude = trajectory.points[end-1].lat;
			Vessel_Longitude = trajectory.points[end-1].lon;
			Vessel_Speed = trajectory.points[end-1].speed;
			Vessel_Course = trajectory.points[end-1].course;
			return;
		}
		else {
			coord curr = trajectory.points[trajectoryPos];
			coord next = trajectory.points[trajectoryPos + 1];
			double speed = (curr.speed + next.speed) / 2.0;
			double course = (curr.course + next.course) / 2.0;
			pair<double, double> direction = LatLonToXY(curr.lat, curr.lon, next.lat, next.lon);
			direction.first = direction.first / sqrt(direction.first * direction.first + direction.second * direction.second);
			direction.second = direction.second / sqrt(direction.first * direction.first + direction.second * direction.second);

			double dx = knotToKmPerMin(adjustZeroSpeed(speed)) * direction.first *mt;
			double dy = knotToKmPerMin(adjustZeroSpeed(speed)) * direction.second *mt;
			pair<double, double> new_lat_lon = XYToLatLon(Vessel_Latitude, Vessel_Longitude, dx, dy);
			Vessel_Latitude = new_lat_lon.first;
			Vessel_Longitude = new_lat_lon.second;
			Vessel_Speed = speed;
			Vessel_Course = course;
			timeToNext = timeToNext - mt;
		}

	}
	if (trajectoryPos >= end - 1) { // end of point
									//remain unchanged
		endPoint = true;
	}
}

void OwnVessel::updateTime(double loopTime)
{
	trajectoryPos++;
	timeToNext = trajectory.timeVect[trajectoryPos + 1] - trajectory.timeVect[trajectoryPos];
}

void OwnVessel::updateTrajectory()
{
	// add current position as a new point in trajectory
	vector<coord> own = trajectory.points;
	vector<double> time = trajectory.timeVect;
	int pos = trajectoryPos+1;
	double ttn = time[pos] - timeToNext;
	coord newpt(Vessel_Latitude, Vessel_Longitude, Vessel_Course, Vessel_Speed);
	own.insert(own.begin() + pos, newpt);
	time.insert(time.begin() + pos, ttn);
	// update time to next and trajectory pos
	trajectoryPos++;
	timeToNext = trajectory.timeVect[trajectoryPos+1] - trajectory.timeVect[trajectoryPos];
}

pair<double, double> OwnVessel::LatLonToXY(double lat1, double lon1, double lat2, double lon2)
{
	double dx = (lon2 - lon1) * 40000 * cos((lat1 + lat2)* PI / 360) / 360; // approx using mid point of lat
	double dy = (lat2 - lat1) * 40000 / 360;
	return pair<double, double>(dx, dy);
}

double OwnVessel::adjustZeroSpeed(double knot)
{
	if (knot <= 0.1) {
		return 0.1;
	}
	else {
		return knot;
	}
}

double OwnVessel::knotToKmPerMin(double knot)
{
	return knot * 1.85200 / 60.0;
}

pair<double, double> OwnVessel::XYToLatLon(double reference_lat, double reference_lon, double x, double y)
{
	// x, y coordinate are such that rightwards is positive x axis and upwards is positive y axis
	double lat2 = reference_lat + y * 360 / 40000;
	double lon2 = reference_lon + x / (40000 * cos((reference_lat + lat2)* PI / 360) / 360);
	return pair<double, double>(lat2, lon2);
}

void OwnVessel::setOwnTrajectory(OwnTrajectory own)
{
	trajectory = own;
}

OwnTrajectory OwnVessel::getOwnTrajectory()
{
	return trajectory;
}

void OwnVessel::setTrajectoryPos(int pos)
{
	trajectoryPos = pos;
}

int OwnVessel::getTrajectoryPos()
{
	return trajectoryPos;
}

void OwnVessel::writeToOutStreams(double old_speed, double old_course, std::ofstream & outSpeedF, std::ofstream & outCourseF, double runtime, std::string avoidanceStr, std::ostream & OwnTargetHistory, std::ostream & outTargetHistory)
{
	//// log speed
	//double schange = this->getSpeed() - old_speed;
	//double newSpeed = this->getSpeed();
	//Randspeed aRandspeed(this->getName(), old_speed, schange, newSpeed);
	//aRandspeed.outRandspeed(outSpeedF);
	//this->setSpeed(newSpeed);

	//// log course
	//double cchange = this->getCourse() - old_course;
	//double newCourse = this->getCourse();
	//if (newCourse > 360)
	//	newCourse = double(int(newCourse) % 360);
	//while (newCourse < 0) {
	//	newCourse = 360 + newCourse;
	//}
	//Randcourse aRandcourse(this->getName(), this->getCourse(), cchange, newCourse);
	//aRandcourse.outRandcourse(outCourseF);
	//this->setCourse(newCourse);

	//double newLongitude = this->getLongitude();
	//double newLatitude = this->getLatitude();
	//this->setLongitude(newLongitude);
	//this->setLatitude(newLatitude);
	//if (strcmp(avoidanceStr.c_str(), "NO"))
	//{
	//	if (this->isOwnVessel == 1)
	//	{
	//		//Save subsequent covered locations of the Own vessel in the Own Vessel History
	//		if (strcmp(avoidanceStr.c_str(), "NULL"))
	//		{
	//			OwnTargetHistory << avoidanceStr << " ";
	//		}
	//		OwnTargetHistory << " " << newLongitude << " " << newLatitude << " " << runtime << std::endl;
	//	}
	//	else
	//	{
	//		//Save subsequent covered locations by the target vessels in Target vessels History

	//		// course to log is decided by either actual course used, or on pattern course
	//		double on_pattern_course = (*this->getPattern().getPoints())[this->on_pattern_pos].getCourseOverGround();
	//		double actual_course;
	//		if (this->getOnPatternPos() + 2 < this->getPattern().getTrajectoryLength()) {
	//			// Compare the on pattern course with v.getOnPatternPos() to v.getOnPatternPos() + 2
	//			pair<double, double> direction = PathMoverTrajectoryBased::LatLonToXY(
	//				(*this->getPattern().getPoints())[this->getOnPatternPos()].getLatitude(),
	//				(*this->getPattern().getPoints())[this->getOnPatternPos()].getLongitude(),
	//				(*this->getPattern().getPoints())[this->getOnPatternPos() + 2].getLatitude(),
	//				(*this->getPattern().getPoints())[this->getOnPatternPos() + 2].getLongitude()
	//				);
	//			// normalize direction vector
	//			direction.first = direction.first / sqrt(direction.first * direction.first + direction.second * direction.second);
	//			direction.second = direction.second / sqrt(direction.first * direction.first + direction.second * direction.second);
	//			actual_course = 90 - (atan2(direction.second, direction.first) * 180 / PI);
	//			if (actual_course < 0) {
	//				actual_course += 360;
	//			}
	//		}
	//		else {
	//			actual_course = this->getCourse();
	//		}
	//		double course_to_log;
	//		if (this->getPattern().getId() == 123) { // force logging actual course for the pattern with not so good course learned
	//			course_to_log = actual_course;
	//		}
	//		else if (abs(on_pattern_course - actual_course) < 80) {
	//			course_to_log = on_pattern_course;
	//		}
	//		else {
	//			course_to_log = actual_course;
	//		}
	//		outTargetHistory
	//			<< " " << (this->getPattern().getId())
	//			<< " " << this->getOnPatternPos()
	//			<< "/" << this->getPattern().getTrajectoryLength()
	//			<< " " << newLongitude << " " <<
	//			newLatitude << " " << course_to_log << " " << runtime << std::endl;
	//	}
	//}
	double newLongitude = Vessel_Longitude;
	double newLatitude = Vessel_Latitude;
	outTargetHistory
		<< " " << "own"
		<< " " << 0
		<< " " << 0
		<< "/" << 0
		<< " " << newLongitude << " " <<
		newLatitude << " " << this->getCourse() << " " << runtime << std::endl;
}
